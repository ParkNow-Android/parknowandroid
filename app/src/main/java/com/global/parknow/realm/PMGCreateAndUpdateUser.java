package com.global.parknow.realm;

import android.app.Activity;

import com.global.parknow.models.PMGUser;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by shwetha.v on 6/26/2015.
 */
public class PMGCreateAndUpdateUser {

    private final Activity mActivity;
    private Realm mRealm;

    public PMGCreateAndUpdateUser(Activity mActivity) {
        this.mActivity = mActivity;
    }


    //   remove the userid if user chooses not to save login credentials/if user updates the new Id
    public void UpdateUserId() {
        mRealm = Realm.getInstance(mActivity);
        RealmResults<PMGUser> result;
        result = getResult();
        if(result.size() != 0) {
            result.first();
            for(int i = 0 ; i <= result.size();i++) {
                result.remove(i);
            }
        }
        mRealm.commitTransaction();

    }

    //    get the user data from realm db
    public RealmResults<PMGUser> getResult(){
        mRealm = Realm.getInstance(mActivity);
        mRealm.beginTransaction();
        RealmQuery<PMGUser> query = mRealm.where(PMGUser.class);
        query.notEqualTo("email","");
        return query.findAll();
    }

    //    check if the userid is saved before
    public String CheckforUserId() {
        mRealm = Realm.getInstance(mActivity);
        String email = "";
        RealmResults<PMGUser> result = getResult();
        if(result.size() != 0) {
            result.first();
            email = result.get(0).getEmail();
        }
        mRealm.commitTransaction();
        return email;
    }

    //    Create a user object if record doesn't exist
    public void createUserObject(String s) {
        mRealm = Realm.getInstance(mActivity);
        mRealm.beginTransaction();
        PMGUser user = mRealm.createObject(PMGUser.class); // Create a new object
        user.setEmail(s);
        mRealm.commitTransaction();
    }
}
