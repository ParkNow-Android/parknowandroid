package com.global.parknow.util;

import android.util.Base64;

import java.io.UnsupportedEncodingException;

/**
 * Created by shwetha.v on 6/18/2015.
 */
public class PMGConvertstring {

    public String ConvertStringTobase64(String Username, final String Password) {

        String mAuthorizationheader = Username + ":" + Password;
        String base64 = null;
        byte[] data = new byte[0];
        try {
            data = mAuthorizationheader.getBytes("UTF-8");
            base64 = Base64.encodeToString(data, Base64.DEFAULT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return base64;
    }


}
