package com.global.parknow.util;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.widget.Toast;

import com.global.parknow.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by shwetha.v on 6/25/2015.
 */
public class PMGLoadMap {
    private final Context mContext;
    private GoogleMap mGoogleMap;
    LocationManager locationManager;

    public PMGLoadMap(Context mContext) {
        this.mContext = mContext;
    }

    public void initializeMap(MapView mMapView,boolean mMapSupported) {

        if (mGoogleMap == null && mMapSupported) {
            mGoogleMap = mMapView.getMap();

            if (mGoogleMap != null) {
                mGoogleMap.setMyLocationEnabled(true);
            }

            locationManager = (LocationManager) mContext
                    .getSystemService(mContext.LOCATION_SERVICE);

            // Creating a criteria object to retrieve provider
            Criteria criteria = new Criteria();

            // Getting the name of the best provider
            String provider = locationManager.getBestProvider(criteria, true);

            // Getting Current Location
            Location location = locationManager.getLastKnownLocation(provider);

            if(location!=null) {

                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(location.getLatitude(), location.getLongitude()), 13));

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        // Sets the center of the map to location user
                        .target(new LatLng(location.getLatitude(), location.getLongitude()))
                        .zoom(19)                   // Sets the zoom
                        .build();                   // Creates a CameraPosition from the builder
                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            }
        } else {
            Toast.makeText(mContext.getApplicationContext(),
                    mContext.getResources().getString(R.string.unable_to_load_map),
                    Toast.LENGTH_LONG).show();
        }
    }
}
