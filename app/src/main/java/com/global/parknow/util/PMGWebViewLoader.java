package com.global.parknow.util;

import android.webkit.WebView;

/**
 * Created by shwetha.v on 6/22/2015.
 */
public class PMGWebViewLoader {

    public static void loadWebView(WebView mWebView,String mUrl){
        mWebView.loadUrl(mUrl);
    }
}
