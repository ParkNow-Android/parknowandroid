package com.global.parknow.util;

import android.content.Context;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by shwetha.v on 6/26/2015.
 */
public class PMGValidateEditText {

    private final Context mContext;


    public PMGValidateEditText(Context mContext) {
        this.mContext = mContext;
    }

//    validate editText fields
    public boolean Validate(EditText editText, String entry,String message) {
        boolean mIsValid = true;

        if (entry.length() <= 0) {
            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
            editText.requestFocus();
            mIsValid = false;
        }
        return mIsValid;
    }
}
