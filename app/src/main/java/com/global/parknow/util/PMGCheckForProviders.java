package com.global.parknow.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.widget.Toast;

import com.global.parknow.R;

/**
 * Created by shwetha.v on 6/26/2015.
 */
public class PMGCheckForProviders extends PhoneStateListener {
    private final Activity mActivity;

    public PMGCheckForProviders(Activity activity) {
        this.mActivity = activity;
    }

//    checking for the GPS availability
    public void checkForLocationManager() {
        LocationManager locationManager = (LocationManager)mActivity.getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
             alertDialogue("LocationManager",mActivity.getResources().getString(R.string.gps_not_found),
                     mActivity.getResources().getString(R.string.enable_location));

    }

//    checking for the network/internet availability
    public boolean checkForNetworkManager() {
        boolean connected;
        ConnectivityManager connectivityManager;

        connectivityManager = (ConnectivityManager) mActivity .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        connected = networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
            if(!connected)
                /*alertDialogue("NetworkManager",mActivity.getResources().getString(R.string.connection_manager),
                        mActivity.getResources().getString(R.string.enable_internet));*/
                Toast.makeText(mActivity.getApplicationContext(),
                        mActivity.getResources().getString(R.string.enable_internet),
                        Toast.LENGTH_LONG).show();
        return connected;
    }

// display alert dialogue
    private void alertDialogue(final String function,String title,String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(mActivity.getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(function.equalsIgnoreCase("LocationManager")){
                            mActivity.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }else if(function.equalsIgnoreCase("NetworkManager")){
                            mActivity.startActivity(new Intent(Settings.ACTION_SETTINGS));
                        }

                        dialogInterface.dismiss();
                    }
                }).setNegativeButton(mActivity.getResources().getString(R.string.no), null);
        builder.create().show();
    }
    }

