package com.global.parknow.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by shwetha.v on 6/18/2015.
 */
public class PMGDateConversion {
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    //    returns the standard UTC time based on the device time
    public String GetUTCdatetimeAsString()
    {
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(new Date());
    }

    //    returns the time difference between two input time strings
    public boolean getTimeDifference(String mInputString1,String mInputString2){
        boolean isGreater = false;
        try {
            Date mConvertedTime1 = dateFormat.parse(mInputString1.substring(11));
            Date mConvertedTime2 = dateFormat.parse(mInputString2);

            if(mConvertedTime1.before(mConvertedTime2))
                isGreater = true;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return isGreater;
    }
}
