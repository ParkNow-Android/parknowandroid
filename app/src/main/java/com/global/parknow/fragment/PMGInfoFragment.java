package com.global.parknow.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.global.parknow.R;
import com.global.parknow.adapter.PMGAlertDialogListAdapter;
import com.global.parknow.adapter.PMGInfoListAdapter;
import com.global.parknow.models.PMGAlertDialogRowItems;

import java.util.ArrayList;

public class PMGInfoFragment extends Fragment {
    private Context mContext;

    private String[] mAlertItems = null;
    private final int[] mFeedbackImages = {R.mipmap.love_icon, R.mipmap.report_incorrect_icon, R
            .mipmap
            .help_icon, R.mipmap.idea_icon, R.mipmap.cancel_icon};

    private final int[] mMenuImages = {R.mipmap.report_incorrect_icon, R.mipmap.how_works_icon, R
            .mipmap
            .faq_icon, R.mipmap.terms_and_conditions_icon};

    private final ArrayList<PMGAlertDialogRowItems> mInfoListViewValues = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        String[] mMenuStrings = getResources().getStringArray(R.array.info_list_items);
        mAlertItems = getResources().getStringArray(R.array.info_alert_content);
        final View view_Info = inflater.inflate(R.layout.info_screen, container, false);

        mContext = getActivity().getApplicationContext();

        ListView listView_InfoMenu = (ListView) view_Info.findViewById(R.id.info_screen_listView_listInfoMenu);
        listView_InfoMenu.setAdapter(new PMGInfoListAdapter(mMenuStrings, mContext, mMenuImages));

        LinearLayout linearLayout_feedback = (LinearLayout) view_Info.findViewById(R.id.info_screen_linearLayout_feedback);
        setAlertListData();

        linearLayout_feedback.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showDialogListView(container);
            }
        });
        return view_Info;
    }

    private void showDialogListView(ViewGroup container) {

        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view = layoutInflater.inflate(R.layout.alertdialog_listview, container, false);
        PMGAlertDialogListAdapter mAlertDialogListAdapter;

        //create a dialog to show a list

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setCancelable(true);
        final AlertDialog dialog = builder.create();
//        dialog.getWindow().setGravity(GravityCompat.);
        dialog.show();
        //create an instance of ListView
        ListView listView_alert = (ListView) view.findViewById(R.id.alertdialog_listView);

        mAlertDialogListAdapter = new PMGAlertDialogListAdapter(mContext, mInfoListViewValues);
        listView_alert.setAdapter(mAlertDialogListAdapter);

        //set OnItemClickListener
        listView_alert.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {

                dialog.cancel();
            }

        });

    }

    private void setAlertListData() {

        for (int i = 0; i < mAlertItems.length; i++) {
            final PMGAlertDialogRowItems dialogItems = new PMGAlertDialogRowItems();
            dialogItems.setContent(mAlertItems[i]);
            dialogItems.setImageId(mFeedbackImages[i]);
            mInfoListViewValues.add(dialogItems);
        }

    }

 }
