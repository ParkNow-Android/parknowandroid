package com.global.parknow.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;

import android.support.v4.app.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.global.parknow.R;

import com.global.parknow.activity.PMGNotificationActivity;
import com.global.parknow.adapter.PMGAlertDialogListAdapter;
import com.global.parknow.adapter.PMGSettingAlertListAdapter;
import com.global.parknow.models.PMGAlertDialogRowItems;
import com.global.parknow.models.PMGSettingListModel;

import java.util.ArrayList;

public class PMGSettingFragment extends Fragment
{

    private CommunicationChannel mCommChListner = null;

    private TextView textView_languageTitle;
    private ImageView imageView_lang_flag;
    private TextView textView_accountType;
    private TextView textView_viewNotification;
    private TextView textView_lang_langText;
    private TextView textView_defaultparking;
    private TextView textView_other;


    private final ArrayList<PMGSettingListModel> mSettingListViewValues = new ArrayList<>();

    private String[] mListValue;
    int mFlagIconId;

    //private Locale mLocale;
    private ViewGroup mViewGroup;

    private Context mContext = null;
    private String[] mAlert_items = null;

    //For Language values.
    private String[] mLangListValue = null;

    public static final String PREFS_NAME = "Language_Setting";
    private final int[] mAlert_icons = {R.mipmap.account_icon, R.mipmap.account_icon, R.mipmap
            .cancel_icon};
    //For Flag icons.
    private final int[] mLangFlagListValue = {R.mipmap.england_flag_icon, R.mipmap.germany_flag_icon};

    private final ArrayList<PMGAlertDialogRowItems> mCustomListView_Values = new ArrayList<>();
    private final ArrayList<PMGAlertDialogRowItems> mCustomListView_langValues = new ArrayList<>();
    View settings = null;
    PMGSettingFragment mCustomListView = null;
    ListView listView_alert = null;
    RelativeLayout relativeLayout_Language = null;
    PMGSettingAlertListAdapter mAdapter = null;
    String selectedValue = null;
    int selectedPosition = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {

        Log.d("Setting", "OnCreateView with Views.");
        settings = inflater.inflate(R.layout.settings_tab, container, false);
        mCustomListView = this;

        mViewGroup = container;
        mContext = mViewGroup.getContext();

        mAlert_items = mContext.getResources().getStringArray(R.array.account_type);

        mListValue = getResources().getStringArray(R.array.setting_list_row_array);

        mLangListValue = getResources().getStringArray(R.array.setting_list_county_array);

        listView_alert = (ListView) settings.findViewById(R.id.settings_tab_listView_alert);
        relativeLayout_Language = (RelativeLayout) settings.findViewById(R.id
                .setting_tab_relativeLayout_language);

        textView_viewNotification = (TextView) settings.findViewById(R.id.setting_tab_textView_viewNotification);
        textView_accountType = (TextView) settings.findViewById(R.id.setting_tab_textView_account_type);
        textView_other = (TextView) settings.findViewById(R.id.setting_tab_textView_other);
        textView_languageTitle = (TextView) settings.findViewById(R.id.setting_tab_textView_lang);
        imageView_lang_flag = (ImageView) settings.findViewById(R.id.setting_tab_imageView_flagImg);
        textView_lang_langText = (TextView) settings.findViewById(R.id
                .setting_tab_textView_langText);
        textView_defaultparking = (TextView) settings.findViewById(R.id.setting_tab_textView_defaultparking);
        textView_defaultparking.setText(R.string.settings_default_parking_text);
        textView_viewNotification.setText(R.string.settings_view_notification_text);
        textView_other.setText(getString(R.string.settings_other_text));


        return settings;
    }


    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        if (activity instanceof CommunicationChannel) {
            mCommChListner = (CommunicationChannel) activity;
        }
        else {
            throw new ClassCastException();
        }
        Log.d("Setting", "Inside onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Log.d("Setting", "Inside onCreate");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        Log.d("Setting", "Inside onActivityCreated");

        Resources res = getResources();
        setListData();
        setAlertLangListData();
        setAlertListData();

        textView_viewNotification.setOnClickListener(
                new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {

                        Intent intent = new Intent(getActivity(), PMGNotificationActivity.class);
                        getActivity().startActivity(intent);
                    }
                }
        );

        textView_accountType.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showDialogListView(mViewGroup);
            }
        });

        relativeLayout_Language.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showLanguageDialogListView(mViewGroup);

            }
        });


        mAdapter = new PMGSettingAlertListAdapter(mCustomListView, mSettingListViewValues, res);
        listView_alert.setAdapter(mAdapter);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Log.d("Setting", "Inside onStart");
    }

    @Override
    public void onResume()
    {

        Log.d("Setting", "Inside onResume");

        mAlert_items = mContext.getResources().getStringArray(R.array.account_type);

        mListValue = getResources().getStringArray(R.array.setting_list_row_array);

        mLangListValue = getResources().getStringArray(R.array.setting_list_county_array);

        mSettingListViewValues.clear();
        mCustomListView_Values.clear();
        mCustomListView_langValues.clear();

        setListData();
        setAlertLangListData();
        setAlertListData();

//        Resources res = getResources();

//        ListView listView_alert = (ListView) settings.findViewById(R.id.settings_tab_listView_alert);
//        RelativeLayout relativeLayout_Language = (RelativeLayout) settings.findViewById(R.id
//                .setting_tab_relativeLayout_language);

        textView_viewNotification = (TextView) settings.findViewById(R.id.setting_tab_textView_viewNotification);
        textView_accountType = (TextView) settings.findViewById(R.id.setting_tab_textView_account_type);

        textView_languageTitle = (TextView) settings.findViewById(R.id.setting_tab_textView_lang);
        imageView_lang_flag = (ImageView) settings.findViewById(R.id.setting_tab_imageView_flagImg);
        textView_lang_langText = (TextView) settings.findViewById(R.id
                .setting_tab_textView_langText);
        textView_defaultparking = (TextView) settings.findViewById(R.id.setting_tab_textView_defaultparking);
        textView_defaultparking.setText(R.string.settings_default_parking_text);


        textView_viewNotification.setText(R.string.settings_view_notification_text);

        String lang = getLanguagePreferences();
        textView_lang_langText.setText(lang);
        if(lang.equals("English")){
            imageView_lang_flag.setImageResource(mLangFlagListValue[0]);
        }else{
            imageView_lang_flag.setImageResource(mLangFlagListValue[1]);
        }


        /*textView_viewNotification.setOnClickListener(
                new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {

                        Intent intent = new Intent(getActivity(), NotificationActivity.class);
                        getActivity().startActivity(intent);
                    }
                }
        );

        textView_accountType.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showDialogListView(mViewGroup);
            }
        });

        relativeLayout_Language.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showLanguageDialogListView(mViewGroup);

            }
        });*/
        /*langList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                showLanguageDialogListView(view);
            }
        });*/


        super.onResume();

    }

    @Override
    public void onPause()
    {
        super.onPause();
        Log.d("Setting", "Inside onPause");
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        Log.d("Setting", "Inside onDestroyView");
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.d("Setting", "Inside onDestroy");
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        Log.d("Setting", "Inside onDetach");
    }


    private void setListData()
    {


        for (String aMListValue : mListValue) {

            final PMGSettingListModel settingListData = new PMGSettingListModel();

            settingListData.setSetting_list_content(aMListValue);
            settingListData.setSetting_list_image("greyarrow");//passing the name of image as a String.

            mSettingListViewValues.add(settingListData);
        }
    }


    private void showDialogListView(ViewGroup viewGroup)
    {

        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view_Alert = layoutInflater.inflate(R.layout.alertdialog_listview, viewGroup, false);

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());


        //create an instance of ListView
        ListView listView_Alert = (ListView) view_Alert.findViewById(R.id.alertdialog_listView);

        PMGAlertDialogListAdapter mAlertAdapter = new PMGAlertDialogListAdapter(mContext, mCustomListView_Values);
        listView_Alert.setAdapter(mAlertAdapter);

        builder.setView(view_Alert);
        builder.setCancelable(true);
        final AlertDialog dialog = builder.create();


        dialog.show();

        //set OnItemClickListener
        listView_Alert.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
//                String selectedItem = ((TextView) view_Alert.findViewById(R.id.alert_content_text)).getText().toString();
                if (position != 2) {
                    if (position == 0) {
                        textView_accountType.setText(R.string.account_type_business);
                    }
                    else if (position == 1) {
                        textView_accountType.setText(R.string.account_type_private);
                    }
                }
                dialog.cancel();
            }
        });
    }

    private void showLanguageDialogListView(ViewGroup viewGroup)
    {

        LayoutInflater layoutInflater_language = LayoutInflater.from(getActivity());
        View view_Alert_Language = layoutInflater_language.inflate(R.layout.alertdialog_listview, viewGroup, false);


        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());


        //create an instance of ListView
        ListView listView_Alert = (ListView) view_Alert_Language.findViewById(R.id.alertdialog_listView);

        PMGAlertDialogListAdapter mLangAlertAdapter = new PMGAlertDialogListAdapter(mContext, mCustomListView_langValues);
        listView_Alert.setAdapter(mLangAlertAdapter);

        builder.setView(view_Alert_Language);
        builder.setCancelable(true);
        final AlertDialog dialog = builder.create();
        dialog.show();

//        final int selectedPosition;
        //set OnItemClickListener
        listView_Alert.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                String selectedItem = ((TextView) view.findViewById(R.id.alertdialog_list_textView_content)).getText()
                        .toString();
                String localeStr = null;

                if (position == 0) {
                    /*imageView_lang_flag.setImageResource(mLangFlagListValue[position]);
                    textView_lang_langText.setText(selectedItem);*/
                    localeStr = "en";
                }
                else if (position == 1) {
                    localeStr = "de";
                }
                setLanguagePreferences(selectedItem);
                selectedValue = selectedItem;
                selectedPosition = position;

                mCommChListner.setCommunication(localeStr);



                dialog.cancel();
            }
        });
        setLanguageViews(selectedPosition, selectedValue);
    }

    public void setLanguageViews(int position, String text)
    {

        imageView_lang_flag.setImageResource(mLangFlagListValue[position]);
        textView_lang_langText.setText(text);

    }

    private void setAlertListData()
    {

        for (int i = 0; i < mAlert_items.length; i++) {
            final PMGAlertDialogRowItems dialogItems = new PMGAlertDialogRowItems();
            dialogItems.setContent(mAlert_items[i]);
            dialogItems.setImageId(mAlert_icons[i]);
            mCustomListView_Values.add(dialogItems);
        }

    }

    private void setAlertLangListData()
    {

        for (int i = 0; i < mLangListValue.length; i++) {
            final PMGAlertDialogRowItems dialogItems = new PMGAlertDialogRowItems();
            dialogItems.setContent(mLangListValue[i]);
            dialogItems.setImageId(mLangFlagListValue[i]);
            if (!mCustomListView_langValues.contains(dialogItems)) {
                mCustomListView_langValues.add(dialogItems);
            }
        }

    }


    public String getLanguagePreferences()
    {
        SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return settings.getString("selected_language","English");
    }

    public void setLanguagePreferences(String language)
    {
        SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        // Writing data to SharedPreferences
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("selected_language", language);
        Log.d("Selected lang :", language);
        editor.commit();
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        // outState.putSerializable("starttime", startTime);
    }

    //List item on click events.
    public void onItemClick(int mPosition)
    {
        Log.d("Setting", "Clicked Item Position is :" + mPosition);

    }


  /*  public void setLocale(String lang)
    {

        mLocale = new Locale(lang);
        Resources res = getActivity().getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = mLocale;
        res.updateConfiguration(conf, dm);

      *//*  Fragment mFragment = new Setting();
        mFragment.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack


*//**//*        transaction.replace(R.id.fragment_container, mFragment);
*//**//*

//        transaction.addToBackStack(null);
        // transaction.commitAllowingStateLoss();
        transaction.detach(mFragment);
        transaction.attach(mFragment);
        // Commit the transaction
        transaction.commit();*//*

       *//* getActivity().finish();

        Intent intent = new Intent(getActivity(),StartActivity.class);

        startActivity(intent);
*//*
    }*/

    public interface CommunicationChannel
    {
        void setCommunication(String msg);
    }
}
