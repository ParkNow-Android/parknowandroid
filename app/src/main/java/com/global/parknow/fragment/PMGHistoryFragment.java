package com.global.parknow.fragment;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.global.parknow.R;
import com.global.parknow.activity.PMGHistorySummaryActivity;
import com.global.parknow.adapter.PMGHistoryListAdapter;
import com.global.parknow.models.PMGHistoryListModel;

import java.util.ArrayList;


public class PMGHistoryFragment extends Fragment
{
    private final ArrayList<PMGHistoryListModel> mHistoryListViewValues = new ArrayList<>();
    public static final int NO_OF_HISTORY_DATA = 2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {

        View history = inflater.inflate(R.layout.history_tab, container, false);
        PMGHistoryFragment customListView = this;


        //Here check if any data is available in the ArrayList or not if not then use history tab textview and set it to no data found.

        setListData(NO_OF_HISTORY_DATA);

        Resources res = getResources();
        ListView listView_History = (ListView) history.findViewById(R.id.history_listView);

        /**************** Create Custom Adapter *********/
        PMGHistoryListAdapter adapter = new PMGHistoryListAdapter(customListView, mHistoryListViewValues, res);
        listView_History.setAdapter(adapter);

        return history;
    }


    /**
     * *** Function to set data in ArrayList ************
     */
    private void setListData(int mNoOfData)
    {

        for (int i = 0; i < mNoOfData; i++) {

            final PMGHistoryListModel historyData = new PMGHistoryListModel();

            /****** Firstly take data in model object *****/
            historyData.setHistory_Parking_Zone(getString(R.string.history_listView_row_zone_txt));
            historyData.setHistory_Parking_Place(getString(R.string.history_listView_row_place_text));
            historyData.setHistory_Parking_day(getString(R.string.history_listView_row_today_txt));
            historyData.setHistory_Parking_Start_Hr(getString(R.string.history_listView_row_startHr_txt));
            historyData.setHistory_Parking_End_Hr(getString(R.string.history_listView_row_endHr_txt));
            historyData.setHistory_Parking_Price(getString(R.string.history_listView_row_price_txt));
            historyData.setHistory_Parking_Vehicle_No(getString(R.string
                    .history_listView_row_vehicelNo_txt));


            /******* Take Model Object in ArrayList *********/
            mHistoryListViewValues.add(historyData);
        }

    }

    public void onItemClick(int mPosition)
    {

//        HistoryListModel tempValues = CustomListViewValues.get(mPosition);
        // SHOW ALERT
        // Toast.makeText(getActivity(),"Position is" + mPosition,Toast.LENGTH_LONG).show();
        Log.d("" + getActivity(), "Position is" + mPosition);
        Intent intent = new Intent(getActivity(), PMGHistorySummaryActivity.class);
        getActivity().startActivity(intent);


    }

  /*  @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
*/
    /* @Override
    public void onDestroyView() {
        super.onDestroyView();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        Fragment fragment = (fm.findFragmentById(R.id.map));
        FragmentTransaction ft = fm.beginTransaction();
        ft.remove(fragment);
        ft.commit();
    }*/
}
