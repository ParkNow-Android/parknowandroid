package com.global.parknow.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import com.global.parknow.R;
import com.global.parknow.activity.PMGLoginActivity;
import com.global.parknow.activity.PMGZoomOutActivity;
import com.global.parknow.adapter.PMGAlertDialogListAdapter;
import com.global.parknow.models.PMGAlertDialogRowItems;
import com.global.parknow.util.PMGCheckForProviders;
import com.global.parknow.util.PMGLoadMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;

import java.util.ArrayList;

public class PMGParkingFragment extends Fragment implements View.OnClickListener {

    private MapView mMapView;
    private boolean mapsSupported = true;
    private ViewGroup mViewGroup;
    PMGLoadMap mLmap;
    PMGCheckForProviders mChkproviders;
    private Context mCustomListView = null;
    private String[] mAlert_items = null;
    private final int[] mAlert_icons = {R.mipmap.signup_icon, R.mipmap.login_icon,
            R.mipmap.payment_icon, R.mipmap.cancel_icon};
    private final ArrayList<PMGAlertDialogRowItems> mParkingListViewValues = new ArrayList<>();


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception e) {
            mapsSupported = false;
        }

        if (mMapView != null) {
            mMapView.onCreate(savedInstanceState);
        }
        mChkproviders = new PMGCheckForProviders(getActivity());
        mChkproviders.checkForLocationManager(); // checking is location providers enabled
        mapinitialiser();
    }

    private void mapinitialiser(){
        mMapView = (MapView) getActivity().findViewById(R.id.parking_tab_mapView_summaryVehicleMap);
        mLmap = new PMGLoadMap(getActivity().getApplicationContext());

        // loads map to the current position and zoom level
        mLmap.initializeMap(mMapView,mapsSupported);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mViewGroup = container;
        View view_Parking = inflater.inflate(R.layout.parking_tab, container, false);
        mCustomListView = container.getContext();
        mMapView = (MapView) view_Parking.findViewById(R.id.parking_tab_mapView_summaryVehicleMap);
        Button button_StartParking = (Button) view_Parking.findViewById(R.id.parking_tab_button_startParking);
        ImageButton imageButton_Zoomout = (ImageButton) view_Parking.findViewById(R.id
                .parking_tab_imageButton_zoomOut);
        ImageButton imageButton_Parking_Action = (ImageButton) view_Parking.findViewById(R.id
                .parking_tab_imageButton_parkingAction);
        mAlert_items = getResources().getStringArray(R.array.start_paking_alert_content);
        ImageButton imageButton_Search = (ImageButton) view_Parking.findViewById(R.id
                .parking_tab_imageButton_serach);

        setAlertListData();

        button_StartParking.setOnClickListener(this);
        imageButton_Zoomout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(), PMGZoomOutActivity
                        .class);
                startActivity(intent);

            }
        });

        imageButton_Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                replacefragment();
            }
        });
        imageButton_Parking_Action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogView(mViewGroup);
            }
        });
        return view_Parking;
    }


    @Override
    public void onClick(View view) {
        showDialogListView(mViewGroup);
    }

    private void showDialogListView(ViewGroup v) {

        LayoutInflater li = LayoutInflater.from(getActivity());
        View view_Alert = li.inflate(R.layout.alertdialog_listview, v, false);

        //dialog to show a list
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view_Alert);
        builder.setCancelable(true);
        final TextView textView_myMsg = new TextView(getActivity());
        textView_myMsg.setText(getResources().getString(R.string.start_parking_alert_title));
        textView_myMsg.setTextSize(11);
        textView_myMsg.setTextColor(Color.BLACK);
        //custom title
        builder.setCustomTitle(textView_myMsg);
        final AlertDialog dialog = builder.create();
        dialog.show();
        //instance of ListView
        ListView listView_Alert = (ListView) view_Alert.findViewById(R.id.alertdialog_listView);

        PMGAlertDialogListAdapter adapter = new PMGAlertDialogListAdapter(mCustomListView, mParkingListViewValues);
        listView_Alert.setAdapter(adapter);

        // OnItemClickListener
        listView_Alert.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1) {
                    Intent intent = new Intent(getActivity(), PMGLoginActivity.class);
                    getActivity().startActivity(intent);
                }
                dialog.cancel();

            }

        });

    }


    private void showDialogView(ViewGroup v) {

        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view = layoutInflater.inflate(R.layout.parking_screen_popup, v, false);

        //dialog to show a layout
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setCancelable(true);
        final AlertDialog dialog = builder.create();
        dialog.show();

    }

//    popup options
    private void setAlertListData() {

        for (int i = 0; i < mAlert_items.length; i++) {
            final PMGAlertDialogRowItems dialogItems = new PMGAlertDialogRowItems();
            dialogItems.setContent(mAlert_items[i]);
            dialogItems.setImageId(mAlert_icons[i]);
            mParkingListViewValues.add(dialogItems);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        mMapView.onResume();
        mapinitialiser();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory()
    {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

}
