package com.global.parknow.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.global.parknow.R;
import com.global.parknow.adapter.PMGInfoListAdapter;
import com.global.parknow.adapter.PMGSearchParkingAdapter;

public class PMGParkingSearchFragment extends Fragment {
    private final String[] mAlert_Items = {"Sign up for ParkNow", "Login with your ParkNow account", "Pay " +
            "via telcoprovider", "Cancel"};
    private final int[] mAlert_Icons = {R.mipmap.signup_icon, R.mipmap.login_icon, R.mipmap.payment_icon, R
            .mipmap.cancel_icon};

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {


            // if(savedInstanceState==null) {
            View view_Parking = inflater.inflate(R.layout.search_parking, container, false);
            ListView listView_Places = (ListView) view_Parking.findViewById(R.id
                    .search_parking_listView_places);
            PMGInfoListAdapter mInfoListAdapter = new PMGInfoListAdapter(mAlert_Items, getActivity().getApplicationContext(), mAlert_Icons);
            listView_Places.setAdapter(mInfoListAdapter);

            ListView listView_Spots = (ListView) view_Parking.findViewById(R.id.search_parking_listView_spots);
            PMGSearchParkingAdapter mSearchParkingAdapter = new PMGSearchParkingAdapter(mAlert_Items, mAlert_Items,
                    getActivity().getApplicationContext(), mAlert_Icons, mAlert_Icons);
            listView_Spots.setAdapter(mSearchParkingAdapter);

            return view_Parking;
        }
}
