package com.global.parknow.api;

import com.global.parknow.models.PMGAuthenticationLogin;
import com.global.parknow.models.PMGRefreshTokenBean;
//import com.global.parknow.models.RetrievePassword;
import com.global.parknow.models.PMGRetrievePasswordModel;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;

public interface PMGApiInterface {

  @Headers({
           "Accept: application/json",
           "Content-Type: application/json"
   })

    @POST("/message/resetpassword")
     void getRetrievePassword(@Body PMGRetrievePasswordObject retrieveEmail ,Callback<PMGRetrievePasswordModel> response);

    @POST("/token")
    void getAuthenticationToken(@Header("Authorization") String encodedAuthentication, Callback<PMGAuthenticationLogin> response);

    @POST("/token/refresh")
    void getNewSessiontoken(@Body PMGRefreshTokenBean refreshtoken,Callback<PMGAuthenticationLogin> response);

}


