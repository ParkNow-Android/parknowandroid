package com.global.parknow.api;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.widget.EditText;

import com.global.parknow.R;
import com.global.parknow.models.PMGAuthenticationLogin;
import com.global.parknow.models.PMGRefreshTokenBean;
import com.global.parknow.models.PMGRetrievePasswordModel;
import com.global.parknow.util.PMGContstants;
import com.global.parknow.util.PMGConvertstring;
import com.global.parknow.util.PMGDateConversion;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by shwetha.v on 6/18/2015.
 */
public class PMGLoginApiCalls {

    private String mUtcTime,mTokenExpTime;
    private boolean isNewTokenNeeded;
    private SharedPreferences msharedpreferences;
    private final Context mContext;
    private String mResultMessage;
    private PMGDateConversion mDateConvert;
    private ProgressDialog mProgressDialog;
    private final Activity mActivity;

    public PMGLoginApiCalls(Context mContext, Activity act) {
        this.mContext = mContext;
        this.mActivity = act;
    }

    //    get session token for the firt time
    public void getFirstSessionToken(String EmailValue, String Password) {
        PMGConvertstring mConvertString = new PMGConvertstring();
        mDateConvert = new PMGDateConversion();

        String mResult = mConvertString.ConvertStringTobase64(EmailValue, Password);
        mProgressDialog =new ProgressDialog(mContext);

        mProgressDialog = ProgressDialog.show(mActivity, null, "Logging in...");

//        API interface to get the response
        PMGApiClient.getsApiInterfaceClient().getAuthenticationToken("Basic "+mResult,
                new Callback<PMGAuthenticationLogin>() {

            @Override
                    public void success(PMGAuthenticationLogin authenticationLogin,
                                        Response response) {
                        setSessionPrefs(authenticationLogin.getToken(),
                                authenticationLogin.getRefreshToken(),
                                authenticationLogin.getTokenExpirationUtc());
                        mUtcTime = mDateConvert.GetUTCdatetimeAsString();
                        mTokenExpTime = authenticationLogin.getTokenExpirationUtc();
                        isNewTokenNeeded = mDateConvert.getTimeDifference
                                (mTokenExpTime, mUtcTime);

                        if (isNewTokenNeeded)
                             getNewSessionToken
                                    (msharedpreferences.getString("RefreshToken",""));
                        else {
                            if(mProgressDialog.isShowing()){
                                mProgressDialog.dismiss();
                            }
                           mActivity.finish();
                        }
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        mResultMessage = checkStatusCode(retrofitError.getResponse()
                                .getStatus(),retrofitError);
                        if(mProgressDialog.isShowing()){
                            mProgressDialog.dismiss();
                        }
                        alertDialogMessage(mResultMessage);
                    }
                });
    }


    //  get session token from the refresh token
    public void getNewSessionToken(String refreshtoken){
        PMGApiClient.getsApiInterfaceClient().getNewSessiontoken(
                new PMGRefreshTokenBean(refreshtoken), new Callback<PMGAuthenticationLogin>() {
                    @Override
                    public void success(PMGAuthenticationLogin authenticationLogin
                            , Response response) {
                        setSessionPrefs(authenticationLogin.getToken(),
                                authenticationLogin.getRefreshToken(),
                                authenticationLogin .getTokenExpirationUtc());
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        mResultMessage = checkStatusCode(retrofitError.getResponse()
                                .getStatus(),retrofitError);
                        alertDialogMessage(mResultMessage);
                    }
                });
    }

    //    assign the values to shared preferences
    private void setSessionPrefs(String mSessiontoken, String mRefreshtoken, String tokenExpirationUtc) {
        msharedpreferences = mContext.getSharedPreferences(new PMGContstants().SESSIONPREFERNCES, mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = msharedpreferences.edit();
        editor.putString("SessionToken",mSessiontoken);
        editor.putString("RefreshToken",mRefreshtoken);
        editor.putString("tokenExpirationUtc",tokenExpirationUtc);
        System.out.println("Refresh Token" +mRefreshtoken );
        editor.apply();
    }

    private String checkStatusCode(int mStatuscode,RetrofitError response){
            String mResult;

        switch(mStatuscode){
            case 304 : mResult = response.getResponse().getReason();
                break;
            case 401 : mResult = response.getResponse().getReason();
                break;
            case 400 :mResult = response.getResponse().getReason();
                break;
            case 403 : mResult = response.getResponse().getReason();
                break;
            case 404 :mResult = response.getResponse().getReason();
                break;
            case 405 : mResult = response.getResponse().getReason();
                break;
            case 500:mResult = response.getResponse().getReason();
                break;
            default:
                mResult = response.getMessage();
                break;
        }

        return mResult;
    }

//    Retrieve password from server
    public void showRetiervePasswordAlertDialog(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mActivity);

        final EditText editText_email= new EditText(mActivity);
        editText_email.setTextColor(mContext.getResources().getColor(R.color.black));
        alertDialog.setMessage(mContext.getResources().getString(R.string.request_login_Message));
        alertDialog.setTitle(mContext.getResources().getString(R.string.request_login_credentials));
        alertDialog.setView(editText_email);

        alertDialog.setPositiveButton(R.string.send, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                mProgressDialog = ProgressDialog.show(mActivity, null,
                        mContext.getResources().getString(R.string.please_wait));
                String mEmail_Phoneno = editText_email.getText().toString();
                if(mEmail_Phoneno.length()==0){
                    if(mProgressDialog.isShowing()){
                        mProgressDialog.dismiss();
                    }
                    alertDialogMessage(mContext.getResources().getString(R.string.email_phoneno));

                }else{

                    PMGApiClient.getsApiInterfaceClient().getRetrievePassword
                            (new PMGRetrievePasswordObject(mEmail_Phoneno),
                                    new Callback<PMGRetrievePasswordModel>() {
                        @Override
                        public void success(PMGRetrievePasswordModel retrievePassword, Response response) {
                            if(mProgressDialog.isShowing()){
                                mProgressDialog.dismiss();
                            }

                            int statusCode = response.getStatus();
                            if(statusCode==201){

                                alertDialogMessage(retrievePassword.getMessage());
                            }else if(statusCode==500){
                                alertDialogMessage(mContext.getResources().getString(R.string.server_error));
                            }
                        }

                        @Override
                        public void failure(RetrofitError retrofitError) {
                            if(mProgressDialog.isShowing()){
                                mProgressDialog.dismiss();
                            }
                            int statusCode = retrofitError.getResponse().getStatus();
                            if(statusCode==403){
                                alertDialogMessage(retrofitError.getResponse().getReason());
                            }else if(statusCode==500){
                                alertDialogMessage(mContext.getResources().getString(R.string.server_error));
                            }

                        }
                    });
                }

            }
        }).setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                mProgressDialog =new ProgressDialog(mContext);
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                dialog.cancel();
            }
        });

        alertDialog.show();

    }

//    general alert message method
   public void alertDialogMessage(String message){
        AlertDialog.Builder alertBuilder=new AlertDialog.Builder(mActivity);
        alertBuilder.setMessage(message);
        alertBuilder.setPositiveButton(mContext.getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertBuilder.create().show();

    }


}
