package com.global.parknow.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.global.parknow.fragment.PMGAccountFragment;
import com.global.parknow.fragment.PMGHistoryFragment;
import com.global.parknow.fragment.PMGInfoFragment;
import com.global.parknow.fragment.PMGParkingFragment;
import com.global.parknow.fragment.PMGSettingFragment;

public class PMGTabPagerAdapter extends FragmentStatePagerAdapter {


    private final CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going
    // to be passed when ViewPagerAdapter is created
    private final int NumbOfTabs; // Store the number of tabs, this will also be passed when the
    // ViewPagerAdapter is created


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public PMGTabPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

    }


    @Override
    public int getItemPosition(Object object)
    {
        return super.getItemPosition(object);
    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                //Fragement for Accounts Tab
                return new PMGAccountFragment();
            case 1:
                //Fragment for Setting Tab
                return new PMGSettingFragment();
            case 2:
                //Fragment for Parkings Tab
                return new PMGParkingFragment();
            case 3:
                //Fragment for HistoryTab
                return new PMGHistoryFragment();
            case 4:
                //Fragment for InfoTab
                return new PMGInfoFragment();

        }
        return null;

    }


    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {

        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {

        return NumbOfTabs; //No of Tabs. You can give your total number of tabs.
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position)
    {
        return super.instantiateItem(container, position);
    }
}
