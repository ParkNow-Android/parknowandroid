package com.global.parknow.adapter;

import android.content.Context;

//import android.content.res.Resources;

import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.global.parknow.R;
import com.global.parknow.fragment.PMGHistoryFragment;
import com.global.parknow.models.PMGHistoryListModel;

import java.util.ArrayList;

public class PMGHistoryListAdapter extends BaseAdapter implements View.OnClickListener
{

    private PMGHistoryFragment mHistoryfragments;
    private ArrayList<PMGHistoryListModel> mHistoryListData;

    public Resources res;
    private static LayoutInflater inflater = null;


    public PMGHistoryListAdapter(PMGHistoryFragment mHistory, ArrayList<PMGHistoryListModel> list, Resources
            resLocal) {
        mHistoryfragments = mHistory;
        mHistoryListData = list;
        res = resLocal;

        /*  Layout inflator to call external xml layout () */
        inflater = (LayoutInflater) mHistoryfragments.getActivity().
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        if (mHistoryListData.size() <= 0)
            return 1;
        return mHistoryListData.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    public static class ViewHolder {

        public TextView textView_zone;
        public TextView textView_zonePlace;
        public TextView textView_day;
        public TextView textView_startHr;
        public TextView textView_endHr;
        public TextView textView_vehicleNo;
        public TextView textView_price;
        //public ImageView img;

    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View vi = view;
        ViewHolder holder;

        if (view == null) {

            /****** Inflate xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.history_listview_row, viewGroup,false);

            /****** View Holder Object to contain xml file elements ******/

            holder = new ViewHolder();
            holder.textView_zone = (TextView) vi.findViewById(R.id.history_listView_row_textView_zone);
            holder.textView_zonePlace = (TextView) vi.findViewById(R.id.history_listView_row_textView_zonePlace);
            holder.textView_day = (TextView) vi.findViewById(R.id.history_listView_row_textView_today);
            holder.textView_startHr = (TextView) vi.findViewById(R.id.history_listView_row_textView_today_startHr);
            holder.textView_endHr = (TextView) vi.findViewById(R.id.history_listView_row_textView_today_endHr);
            holder.textView_price = (TextView) vi.findViewById(R.id.history_listView_row_textView_zonePrice);
            holder.textView_vehicleNo = (TextView) vi.findViewById(R.id
                    .history_listView_row_textView_vehicleNo);

            //holder.image=(ImageView)vi.findViewById(R.id.image);

            /************  Set holder with LayoutInflater ************/
            vi.setTag(holder);
        } else
            holder = (ViewHolder) vi.getTag();

        if (mHistoryListData.size() <= 0) {
            //holder.text.setText("No Data");
            Log.d("HistoryListAdapter","No Data.");


        } else {
            /***** Get each Model object from Arraylist ********/
            PMGHistoryListModel historyList;
            historyList = mHistoryListData.get(position);

            /************  Set Model values in Holder elements ***********/

            holder.textView_day.setText(historyList.getHistory_Parking_day());
            holder.textView_vehicleNo.setText(historyList.getHistory_Parking_Vehicle_No());
            holder.textView_price.setText(historyList.getHistory_Parking_Price());
            holder.textView_endHr.setText(historyList.getHistory_Parking_End_Hr());
            holder.textView_startHr.setText(historyList.getHistory_Parking_Start_Hr());
            holder.textView_zone.setText(historyList.getHistory_Parking_Zone());
            holder.textView_zonePlace.setText(historyList.getHistory_Parking_Place());
            /*holder.image.setImageResource(
                    res.getIdentifier(
                            "com.androidexample.customlistview:drawable/" + historyList.getImage()
                            , null, null));
*/
            /******** Set Item Click Listner for LayoutInflater for each row *******/

            vi.setOnClickListener(new OnItemClickListener(position));
        }


        return vi;
    }

    @Override
    public void onClick(View v) {
        Log.v("HistoryListAdapter", "=====Row button clicked=====");
    }

    /**
     * ****** Called when Item click in ListView ***********
     */
    private class OnItemClickListener implements View.OnClickListener
    {
        private final int mPosition;

        OnItemClickListener(int position) {
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {


            PMGHistoryFragment sct = mHistoryfragments;

            /****  Call  onItemClick Method inside CustomListViewAndroidExample Class ( See Below )****/

            sct.onItemClick(mPosition);
        }
    }
}
