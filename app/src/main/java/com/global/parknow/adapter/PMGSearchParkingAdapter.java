package com.global.parknow.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.global.parknow.R;

public class PMGSearchParkingAdapter extends BaseAdapter
{
    //    Intent mintent;
//    TextView textView;
    String[] mParkingslots = null;
    String[] mCities = null;
    int[] mParkingImageid = null;
    int[] mPrefImgid = null;
    private static LayoutInflater inflater = null;

    public PMGSearchParkingAdapter(String[] mParkingslots, String[] mCities, Context mcontext, int[] mParkingImageid, int[] mPrefImgid)
    {
        this.mParkingslots = mParkingslots;
        this.mCities = mCities;
        this.mParkingImageid = mParkingImageid;
        this.mPrefImgid = mPrefImgid;
        inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount()
    {
        return 0;
    }

   /* private class Holder
    {
        TextView tvParkingName,tvcities;
        ImageView imgpref,parkingimg;
    }*/

    @Override
    public Object getItem(int position)
    {
        return null;
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
//        Holder holder=new Holder();
        return inflater.inflate(R.layout.search_parking_innerview, parent, false);

    }
}
