package com.global.parknow.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.global.parknow.R;
import com.global.parknow.activity.PMGNotificationActivity;
import com.global.parknow.models.PMGNotificationListModel;


import java.util.ArrayList;

public class PMGNotificationInactiveListAdapter extends BaseAdapter implements View.OnClickListener {


    private final ArrayList arrayList;
    private static LayoutInflater inflater = null;
    private final Resources res;

    public PMGNotificationInactiveListAdapter(PMGNotificationActivity notification, ArrayList arrayList,
                                              Resources
                                                      resLocal) {


        this.arrayList = arrayList;
        res = resLocal;

        /*  Layout inflator to call external xml layout () */
        inflater = (LayoutInflater) notification.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }



    @Override
    public int getCount() {
        if (arrayList.size() <= 0)
            return 1;
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public static class ViewHolder {

        public TextView textView_content;
        public ImageView imgageView_next_arrow;
    }


    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View vi = view;
        ViewHolder holder;

        if (view == null) {

            /****** Inflate xml file for each row  *******/
            vi = inflater.inflate(R.layout.notification_inactive_listview_row, viewGroup,false);

            /****** View Holder Object to contain xml file elements ******/

            holder = new ViewHolder();
            holder.textView_content = (TextView) vi.findViewById(R.id
                    .notification_inactive_list_row_textView_msg);
            holder.imgageView_next_arrow = (ImageView) vi.findViewById(R.id
                    .notification_inactive_list_row_imageView_arrowImg);

            /************  Set holder with LayoutInflater ************/
            vi.setTag(holder);
        } else
            holder = (ViewHolder) vi.getTag();

        //holder.text.setText("No Data");
        if (arrayList.size() <= 0) Log.d("NotificationAdapter", "No Data");
        else {
            /***** Get each Model object from Arraylist ********/
            PMGNotificationListModel notificationList;
            notificationList = (PMGNotificationListModel) arrayList.get(position);


            /************  Set Model values in Holder elements ***********/

            holder.textView_content.setText(notificationList.getNotification_inactive_list_content());
            holder.imgageView_next_arrow.setImageResource(
                    res.getIdentifier(
                            "com.global.parknow:mipmap/" + notificationList.getNotification_inactive_list_image()
                            , null, null));


            /******** Set Item Click Listner for LayoutInflater for each row *******/

            vi.setOnClickListener(new OnItemClickListener(position));
        }

        return vi;
    }

    @Override
    public void onClick(View view) {

    }

    private class OnItemClickListener implements View.OnClickListener {
        private final int mPosition;

        OnItemClickListener(int position) {
            mPosition = position;
            Log.d("NotiInactLstAdptr","Clicked Item Position is :"+mPosition);
        }

        @Override
        public void onClick(View arg0) {


        }
    }
}
