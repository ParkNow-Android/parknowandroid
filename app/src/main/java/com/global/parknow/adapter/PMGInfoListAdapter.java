package com.global.parknow.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.global.parknow.R;
import com.global.parknow.activity.PMGInfoActivity;

public class PMGInfoListAdapter extends BaseAdapter {
    private final String [] mresult;
    private final Context mcontext;
    private final int[]  mimageId;
    private Intent mintent;
    private TextView textView_MenuItem;
    private static LayoutInflater inflater=null;

    public PMGInfoListAdapter(String[] mresult, Context mcontext, int[] mimageId) {
        this.mresult = mresult;
        this.mcontext = mcontext;
        this.mimageId = mimageId;
        inflater = ( LayoutInflater )mcontext. getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return mresult.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder
    {
        TextView textView_menuItem;
        ImageView imageView_imgIcon;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Holder holder=new Holder();
        final View rowView;
        rowView = inflater.inflate(R.layout.info_details,parent,false);
        holder.textView_menuItem =(TextView) rowView.findViewById(R.id.info_details_textView_MenuItem);
        holder.imageView_imgIcon =(ImageView) rowView.findViewById(R.id.info_details_imageView_imgicon);

        holder.textView_menuItem.setText(mresult[position]);
        holder.imageView_imgIcon.setImageResource(mimageId[position]);

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mintent = new Intent(mcontext, PMGInfoActivity.class);
                mintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                textView_MenuItem = (TextView) v.findViewById(R.id.info_details_textView_MenuItem);
                String text = textView_MenuItem.getText().toString();
                if(position == 0) {
                    mintent.putExtra("position", 0);
                    mintent.putExtra("title",text); // for setting title bar text
                }else if(position == 1){
                    mintent.putExtra("position",1);
                    mintent.putExtra("title",text); // for setting title bar text
                }else if(position == 2){
                    mintent.putExtra("position",2);
                    mintent.putExtra("title",text); // for setting title bar text
                }else if(position == 3) {
                    mintent.putExtra("position", 3);
                    mintent.putExtra("title", text); // for setting title bar text
                }
                mcontext.startActivity(mintent);
            }
        });
        return rowView;
    }
}
