package com.global.parknow.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.global.parknow.R;
import com.global.parknow.fragment.PMGSettingFragment;
import com.global.parknow.models.PMGSettingListModel;

import java.util.ArrayList;

class PMGSettingLangListAdapter extends BaseAdapter implements View.OnClickListener {


    private final PMGSettingFragment fragment_settings;
    private final ArrayList arrayList;

    private static LayoutInflater inflater = null;
    private final Resources res;

    public PMGSettingLangListAdapter(PMGSettingFragment fragment_settings, ArrayList arrayList, Resources
            res) {


        this.fragment_settings = fragment_settings;
        this.arrayList = arrayList;
        this.res = res;

        /*  Layout inflator to call external xml layout () */
        inflater = (LayoutInflater) fragment_settings.getActivity().
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }



    @Override
    public int getCount() {
        if (arrayList.size() <= 0)
            return 1;
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public static class ViewHolder {

        public TextView textView_content;
        public ImageView imageView_next_arrow;
        public ImageView imageView_county_flag;

    }


    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View vi = view;
        ViewHolder holder;

        if (view == null) {

            /****** Inflate xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.setting_language_list_row, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();
            holder.textView_content = (TextView) vi.findViewById(R.id.setting_lang_list_row_textView_langText);
            holder.imageView_next_arrow = (ImageView) vi.findViewById(R.id.setting_lang_list_row_imageView_next_arrow);
            holder.imageView_county_flag = (ImageView) vi.findViewById(R.id.setting_lang_list_row_imageView_flagImg);





            /************  Set holder with LayoutInflater ************/
            vi.setTag(holder);
        } else
            holder = (ViewHolder) vi.getTag();

        if (arrayList.size() <= 0) {
            //holder.text.setText("No Data");
            Log.d("settinglangadapter", "No Data");


        } else {
            /***** Get each Model object from Arraylist ********/
            PMGSettingListModel settingList;
            settingList = (PMGSettingListModel) arrayList.get(position);


            /************  Set Model values in Holder elements ***********/


                holder.textView_content.setText(settingList.getSetting_lang_list_content());
                holder.imageView_next_arrow.setImageResource(
                        res.getIdentifier(
                                "com.global.parknow:mipmap/" + settingList.getSetting_list_image()
                                , null, null));
                holder.imageView_county_flag.setImageResource(
                        res.getIdentifier(
                                "com.global.parknow:mipmap/" + settingList.getSetting_lang_list_image()
                                , null, null));

            /******** Set Item Click Listner for LayoutInflater for each row *******/

            vi.setOnClickListener(new OnItemClickListener(position));
        }

        return vi;
    }

    @Override
    public void onClick(View view) {

    }

    private class OnItemClickListener implements View.OnClickListener {
        private final int mPosition;

        OnItemClickListener(int position) {
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {


            PMGSettingFragment sct = fragment_settings;


            sct.onItemClick(mPosition);
        }
    }


}
