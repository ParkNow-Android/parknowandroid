package com.global.parknow.adapter;


import android.view.View.OnClickListener;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.global.parknow.R;
import com.global.parknow.activity.PMGNotificationActivity;
import com.global.parknow.models.PMGNotificationListModel;

import java.util.ArrayList;


public class PMGNotificationListAdapter extends BaseAdapter implements OnClickListener {

    private final PMGNotificationActivity notification;
    private final ArrayList arrayList;

    private static LayoutInflater inflater = null;
    private final Resources res;


    public PMGNotificationListAdapter(PMGNotificationActivity notification, ArrayList arrayList, Resources
            resLocal) {


        this.notification = notification;
        this.arrayList = arrayList;
        res = resLocal;

        /*  Layout inflator to call external xml layout () */
        inflater = (LayoutInflater) this.notification.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        if (arrayList.size() <= 0)
            return 1;
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    public static class ViewHolder {

        public TextView textView_content;
        public TextView textView_msg_sms;
        public TextView textView_msg_push;
        public TextView textView_msg_email;
        public ImageView imgageView_next_arrow;

    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View vi = view;
        ViewHolder holder;

        if (view == null) {

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.notification_active_listview_row, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();
            holder.textView_content = (TextView) vi.findViewById(R.id.notification_active_list_row_text_msg);
            holder.textView_msg_email = (TextView) vi.findViewById(R.id.notification_active_list_row_msg_email);
            holder.textView_msg_push = (TextView) vi.findViewById(R.id.notification_active_list_row_msg_push);
            holder.textView_msg_sms = (TextView) vi.findViewById(R.id.notification_active_list_row_msg_sms);
            holder.imgageView_next_arrow = (ImageView) vi.findViewById(R.id.notification_active_list_row_arrow);

            /************  Set holder with LayoutInflater ************/
            vi.setTag(holder);
        } else
            holder = (ViewHolder) vi.getTag();

        if (arrayList.size() <= 0) {
            //holder.text.setText("No Data")
            Log.d("ListAdapter","No Data");

        } else {
            /***** Get each Model object from Arraylist ********/
            PMGNotificationListModel notificationList;
            notificationList = (PMGNotificationListModel) arrayList.get(position);

            /************  Set Model values in Holder elements ***********/

            if (position == 0) {

                holder.textView_msg_push.setVisibility(View.GONE);
                holder.textView_msg_email.setVisibility(View.GONE);
                holder.textView_content.setText(notificationList.getNotification_active_list_content());
                holder.imgageView_next_arrow.setImageResource(
                        res.getIdentifier(
                                "com.global.parknow:mipmap/" + notificationList.getNotification_active_list_image()
                                , null, null));
            } else if (position == 1) {
                holder.textView_msg_sms.setVisibility(View.GONE);
                holder.textView_content.setText(notificationList.getNotification_active_list_content());
                holder.imgageView_next_arrow.setImageResource(
                        res.getIdentifier(
                                "com.global.parknow:mipmap/" + notificationList.getNotification_active_list_image()
                                , null, null));

            } else if (position == 2) {
                holder.textView_msg_sms.setVisibility(View.GONE);
                holder.textView_msg_email.setVisibility(View.GONE);
                holder.textView_content.setText(notificationList.getNotification_active_list_content());
                holder.imgageView_next_arrow.setImageResource(
                        res.getIdentifier(
                                "com.global.parknow:mipmap/" + notificationList.getNotification_active_list_image()
                                , null, null));

            }            /******** Set Item Click Listner for LayoutInflater for each row *******/

            vi.setOnClickListener(new OnItemClickListener(position));
        }


        return vi;
    }

    @Override
    public void onClick(View v) {
        Log.v("HistoryListAdapter", "=====Row button clicked=====");
    }

    /**
     * ****** Called when Item click in ListView ***********
     */
    private class OnItemClickListener implements View.OnClickListener {
        private final int mPosition;

        OnItemClickListener(int position) {
            mPosition = position;

        }

        @Override
        public void onClick(View arg0) {

            System.out.print("Clicked view id is :"+arg0.getId());
                PMGNotificationActivity sct = notification;
                sct.onItemClick(mPosition);

        }
    }
}
