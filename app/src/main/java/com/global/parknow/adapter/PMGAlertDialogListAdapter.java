package com.global.parknow.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.LayoutInflater;

import com.global.parknow.R;
import com.global.parknow.models.PMGAlertDialogRowItems;

import java.util.List;


public class PMGAlertDialogListAdapter extends BaseAdapter {


    private final List<PMGAlertDialogRowItems> rowItems;
    private static LayoutInflater mInflater = null;

    public PMGAlertDialogListAdapter(Context context, List<PMGAlertDialogRowItems> items) {
        final Context mContext;
        mContext = context;
        this.rowItems = items;
        mInflater = (LayoutInflater)
                mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int i) {
        return rowItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        long returnValue;
        returnValue = (long)rowItems.indexOf(getItem(i));
        return returnValue;
    }

    private class ViewHolder {
        ImageView imageView_alertIcon;
        TextView textView_alertContent;

    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        ViewHolder holder;
        View vi = view;

        if (view == null) {
            vi = mInflater.inflate(R.layout.alertdialog_list_row, viewGroup,false);
            holder = new ViewHolder();
            holder.textView_alertContent = (TextView) vi.findViewById(R.id.alertdialog_list_textView_content);
            holder.imageView_alertIcon = (ImageView) vi.findViewById(R.id.alertdialog_list_imageView_icon);
            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }

        PMGAlertDialogRowItems rowItem = (PMGAlertDialogRowItems) getItem(position);

        holder.textView_alertContent.setText(rowItem.getContent());
        holder.imageView_alertIcon.setImageResource(rowItem.getImageId());


        return vi;
    }
}
