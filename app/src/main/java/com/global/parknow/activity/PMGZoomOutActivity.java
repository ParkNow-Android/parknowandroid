package com.global.parknow.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageButton;

import com.global.parknow.R;
import com.global.parknow.util.PMGCheckForProviders;
import com.global.parknow.util.PMGLoadMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;


public class PMGZoomOutActivity extends Activity {
    private MapView mMapView;
    private boolean mapsSupported = true;
    private PMGLoadMap mLmap;
    private PMGCheckForProviders mCheckProviders;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoomout);
        mMapView = (MapView) findViewById(R.id.zoomout_mapView_loc_Map);
        mCheckProviders = new PMGCheckForProviders(this);
        try {
            MapsInitializer.initialize(PMGZoomOutActivity.this); // initialises map here
        } catch (Exception e) {
            mapsSupported = false;
        }

        if (mMapView != null) {
            mMapView.onCreate(savedInstanceState);
        }

        mCheckProviders.checkForLocationManager(); // checking is location providers enabled
        mapinitialiser();
        ImageButton imgageButton_Zoomin = (ImageButton) findViewById(R.id.zoomout_imageButton_zoomIn);
        imgageButton_Zoomin.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finish();
            }

        });


    }

    private void mapinitialiser(){
        mLmap = new PMGLoadMap(getApplicationContext());
        // loads map to the current position and zoom level
        mLmap.initializeMap(mMapView,mapsSupported);
    }

    @Override
    @NonNull
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        mMapView.onResume();
        mapinitialiser();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory()
    {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

}
