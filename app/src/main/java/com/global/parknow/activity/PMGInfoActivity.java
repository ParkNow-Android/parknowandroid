package com.global.parknow.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;

import com.global.parknow.R;

import static com.global.parknow.util.PMGWebViewLoader.loadWebView;

public class PMGInfoActivity extends AppCompatActivity {
    Toolbar toolbar;
    String mTitle;
    int mPosition;
    WebView mWebView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        mTitle = getIntent().getExtras().getString("title");
        setTitle(mTitle);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        mWebView = (WebView) findViewById(R.id.info_webView_info);

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.arrow_back_white_icon);

        mPosition = getIntent().getExtras().getInt("position", 0);

        switch (mPosition){
            case 0 :
                loadWebView(mWebView, "http://www.parkmobile.co.uk/m/uk/termsandconditions");
                break;
            case 1 :
                loadWebView(mWebView, "http://www.parkmobile.co.uk/m/uk/termsandconditions");
                break;
            case 2 :
                loadWebView(mWebView, "http://www.parkmobile.co.uk/m/uk/termsandconditions");
                break;
            case 3 :
                loadWebView(mWebView, "http://www.parkmobile.co.uk/m/uk");
                break;
            default:
                break;
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

    }
}
