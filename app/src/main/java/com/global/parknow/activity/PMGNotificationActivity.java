package com.global.parknow.activity;


import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.global.parknow.R;
import com.global.parknow.adapter.PMGNotificationInactiveListAdapter;
import com.global.parknow.adapter.PMGNotificationListAdapter;
import com.global.parknow.models.PMGNotificationListModel;

import java.util.ArrayList;

public class PMGNotificationActivity extends AppCompatActivity {


    private final ArrayList<PMGNotificationListModel> CustomListViewValues = new ArrayList<>();
    private final ArrayList<PMGNotificationListModel> CustomListViewInactiveValues = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        //To add toolbar to the screen
        setSupportActionBar(toolbar);
        //To add back button to Toolbar.
        toolbar.setNavigationIcon(R.mipmap.arrow_back_white_icon);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });
        PMGNotificationActivity customListView = this;

        setListData();

        Resources res = getResources();

        ListView activeList = (ListView) findViewById(R.id.notification_listView_active);
        ListView inactiveList = (ListView) findViewById(R.id.notification_listView_inactive);

       /* textView_email = (TextView) findViewById(R.id.notification_active_list_row_msg_email);
        textView_sms = (TextView) findViewById(R.id.notification_active_list_row_msg_sms);
        textView_push = (TextView) findViewById(R.id.notification_active_list_row_msg_push);
        imageView_arrow = (ImageView) findViewById(R.id.notification_active_list_row_arrow);

        textView_inactive_msg = (TextView) findViewById(R.id.notification_inactive_list_row_text_msg);
        imageView_inactive_arrow = (ImageView) findViewById(R.id.notification_inactive_list_row_arrow_img);
*/

        PMGNotificationListAdapter adapter = new PMGNotificationListAdapter(customListView, CustomListViewValues, res);
        activeList.setAdapter(adapter);

        PMGNotificationInactiveListAdapter inactiveAdapter = new PMGNotificationInactiveListAdapter(customListView, CustomListViewInactiveValues, res);
        inactiveList.setAdapter(inactiveAdapter);


    }

    private void setListData() {

        String[] mListValue = getResources().getStringArray(R.array.notification_active_list_row_array);
        String[] mInactiveListValue = getResources().getStringArray(R.array.notification_inactive_list_array);

        for (String aMListValue : mListValue) {

            final PMGNotificationListModel notificationListData = new PMGNotificationListModel();

            notificationListData.setNotification_active_list_content(aMListValue);
            notificationListData.setNotification_active_list_image("greyarrow");//passing the name of image as a String.

            CustomListViewValues.add(notificationListData);
        }

        for (String aMInactiveListValue : mInactiveListValue) {

            final PMGNotificationListModel notificationInactiveListData = new PMGNotificationListModel();

            notificationInactiveListData.setNotification_inactive_list_content(aMInactiveListValue);
            notificationInactiveListData.setNotification_inactive_list_image("greyarrow");//passing the name of image as a String.

            CustomListViewInactiveValues.add(notificationInactiveListData);
        }

    }

    public void onItemClick(int mPosition) {


        if (mPosition == 2) {
            Intent intent = new Intent(getApplicationContext(), PMGReminderActivity.class);
            startActivity(intent);
        }

    }

}
