package com.global.parknow.activity;


import android.app.AlertDialog;
import android.content.Context;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.global.parknow.R;
import com.global.parknow.adapter.PMGAlertDialogListAdapter;
import com.global.parknow.models.PMGAlertDialogRowItems;

import java.util.ArrayList;

public class PMGHistorySummaryActivity extends AppCompatActivity
{

    private TextView textView_accountType;
    Toolbar toolbar;
   /* private GoogleMap mGoogleMap;
    private MapView mMapView;
    private boolean mMapsSupported = true;

*/

    private Context mContext = null;
    private String[] mAlert_items = null;
    private final int[] mAlert_icons = {R.mipmap.account_icon, R.mipmap.account_icon, R.mipmap
            .cancel_icon};
    private final ArrayList<PMGAlertDialogRowItems> mCustomListViewValues = new ArrayList<>();



   /* @Override
    protected void onStart()
    {
        super.onStart();
        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            mMapsSupported = false;
        }

        if (mMapView != null) {
            mMapView.onCreate(savedInstanceState);
        }
        initializeMap();
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_summary);

       /* try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            mMapsSupported = false;
        }

        if (mMapView != null) {
            mMapView.onCreate(savedInstanceState);
        }
        initializeMap();*/


        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        textView_accountType = (TextView) findViewById(R.id.historysummary_textView_accountType);
        mContext = this;

        mAlert_items = mContext.getResources().getStringArray(R.array.account_type);

        setAlertListData();

        //To add toolbar to the screen
        setSupportActionBar(toolbar);
        //To add back button to Toolbar.
        toolbar.setNavigationIcon(R.mipmap.arrow_back_white_icon);

        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

        textView_accountType.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showDialogListView();
            }
        });

//        setStyles();
    }


  /*  public void initializeMap(){

        if (mGoogleMap == null && mMapsSupported) {
            mMapView = (MapView)findViewById(R.id.historysummary_mapView);
            mGoogleMap = mMapView.getMap();
            //setup markers here
        }
    }*/

    private void showDialogListView()
    {

        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View view_Alert_listView = layoutInflater.inflate(R.layout.alertdialog_listview, null);


        //create a dialog to show a list
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view_Alert_listView);
        builder.setCancelable(true);
        final AlertDialog dialog = builder.create();
        dialog.show();
        //create an instance of ListView
        ListView listView_Alert = (ListView) view_Alert_listView.findViewById(R.id.alertdialog_listView);

        PMGAlertDialogListAdapter mAlertAdapter = new PMGAlertDialogListAdapter(mContext, mCustomListViewValues);
        listView_Alert.setAdapter(mAlertAdapter);

        //set OnItemClickListener
        listView_Alert.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {

//                String selectedItem = ((TextView) view_Alert_listView.findViewById(R.id.alert_content_text)).getText().toString();
                if (position != 2) {
                    if (position == 0) {
                        textView_accountType.setText(getResources().getString(R.string.account_type_business));
                    }
                    else if (position == 1) {
                        textView_accountType.setText(getResources().getString(R.string.account_type_private));
                    }
                }
                dialog.cancel();
            }

        });

    }

    private void setAlertListData()
    {

        for (int i = 0; i < mAlert_items.length; i++) {
            final PMGAlertDialogRowItems dialogItems = new PMGAlertDialogRowItems();
            dialogItems.setContent(mAlert_items[i]);
            dialogItems.setImageId(mAlert_icons[i]);
            mCustomListViewValues.add(dialogItems);
        }

    }


 /*   @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        mMapView.onResume();
        initializeMap();
    }


    @Override
    public void onPause()
    {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }*/
}
