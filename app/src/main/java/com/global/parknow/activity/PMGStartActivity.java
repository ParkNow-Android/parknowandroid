package com.global.parknow.activity;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.global.parknow.R;
import com.global.parknow.adapter.PMGTabPagerAdapter;
import com.global.parknow.fragment.PMGAccountFragment;
import com.global.parknow.fragment.PMGHistoryFragment;
import com.global.parknow.fragment.PMGInfoFragment;
import com.global.parknow.fragment.PMGParkingFragment;
import com.global.parknow.fragment.PMGSettingFragment;
import com.global.parknow.tabs.PMGSlidingTabLayout;

import java.util.Locale;

import io.fabric.sdk.android.Fabric;

public class PMGStartActivity extends AppCompatActivity implements PMGSettingFragment.CommunicationChannel
{

    //    FrameLayout container;
    Toolbar toolbar;
    PMGTabPagerAdapter mTabPageAdapter;
    public static final int TAB_POSITION_0 = 0;
    public static final int TAB_POSITION_1 = 1;
    public static final int TAB_POSITION_2 = 2;
    public static final int TAB_POSITION_3 = 3;
    public static final int TAB_POSITION_4 = 4;

    public static final int NO_OF_TABS = 5;
    public static final int DEFAULT_TAB_POSITION = 2;
    public static final String PREFS_NAME = "Language_Setting";


    public FragmentManager fragmentManager = null;

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_start);

        CharSequence[] mTitles = getResources().getStringArray(R.array.tab_labels);

        // Creating The Toolbar and setting it as the Toolbar for the activity

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);


        // Creating The TabPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        mTabPageAdapter = new PMGTabPagerAdapter(getSupportFragmentManager(), mTitles, NO_OF_TABS);

        // Assigning ViewPager View and setting the mTabPageAdapter
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(mTabPageAdapter);

        // Assiging the Sliding Tab Layout View
        PMGSlidingTabLayout tabs = (PMGSlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new PMGSlidingTabLayout.TabColorizer()
        {
            @Override
            public int getIndicatorColor(int position)
            {
                return getResources().getColor(R.color.primaryColorDark);
            }
        });

        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
                if (savedInstanceState != null) {

                    fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();


                    Log.d("StartActivity", "Position " + position);
                    if (position == TAB_POSITION_0) {

                        //fragmentTransaction.replace(R.id.start_fragmentContainer, new
                        //      AccountFragment());
                        Fragment fragment = getSupportFragmentManager().findFragmentByTag
                                ("account");
                        if(fragment != null)
                            getSupportFragmentManager().beginTransaction().remove(fragment).commit();

                        fragmentTransaction.add(R.id.start_fragmentContainer, new PMGAccountFragment(),
                                "account");
                    }
                    else if (position == TAB_POSITION_1) {
                        //fragmentTransaction.replace(R.id.start_fragmentContainer, new
                                //SettingFragment());
                        Fragment fragment = getSupportFragmentManager().findFragmentByTag
                                ("settings");
                        if(fragment != null)
                            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                        fragmentTransaction.add(R.id.start_fragmentContainer, new PMGSettingFragment(),
                                "settings");
                    }
                    else if (position == TAB_POSITION_2) {
                        fragmentTransaction.replace(R.id.start_fragmentContainer, new PMGParkingFragment());
                        fragmentTransaction.add(R.id.start_fragmentContainer, new PMGParkingFragment(),
                                "parking");
                    }
                    else if (position == TAB_POSITION_3) {
                        fragmentTransaction.replace(R.id.start_fragmentContainer, new PMGHistoryFragment());
                        fragmentTransaction.add(R.id.start_fragmentContainer, new PMGHistoryFragment(),
                                "history");
                    }
                    else if (position == TAB_POSITION_4) {
                        fragmentTransaction.replace(R.id.start_fragmentContainer, new PMGInfoFragment());
                        fragmentTransaction.add(R.id.start_fragmentContainer, new PMGInfoFragment(),
                                "info");
                    }

                    fragmentTransaction.addToBackStack(null);

                    // Commit the transaction
                    fragmentTransaction.commit();
                }
            }

            @Override
            public void onPageSelected(int position)
            {

            }

            @Override
            public void onPageScrollStateChanged(int state)
            {

            }
        });
        // Setting the TabPager For the SlidingTabsLayout
        tabs.setViewPager(pager);

        if (savedInstanceState != null) {
            int mTabPostion = getIntent().getExtras().getInt("tabPosition");
            int mFlagId = getIntent().getExtras().getInt("flagId");
            String mLanguage = getIntent().getExtras().getString("language");
            Toast.makeText(this, "Tab:" + mTabPostion + "Flag:" + mFlagId + "Language :" + mLanguage, Toast
                    .LENGTH_LONG).show();
            pager.setCurrentItem(mTabPostion);

        }
        else {
            //Setting for displaying parking tab first when application starts.
            pager.setCurrentItem(DEFAULT_TAB_POSITION);
        }

    }

    @Override
    protected void onResume()
    {

     /*   FragmentManager manager = getSupportFragmentManager();
        SettingFragment fragment = (SettingFragment) manager.findFragmentById(R.id.start_fragmentContainer);

      // if()
        String lang =  fragment.getLanguagePreferences();


        Log.d("StartActivity","onResume");
//        String lang = new SettingFragment().getLanguagePreferences();
        Log.i("Language",lang);
        setLocale(lang);*/
        super.onResume();

    }

    @Override
    public void setCommunication(String localeStr)
    {

        Log.d("StartActivity", "in setCommm language is : " + localeStr);
        setLocale(localeStr);
        CharSequence[] mTitles = getResources().getStringArray(R.array.tab_labels);
        mTabPageAdapter = new PMGTabPagerAdapter(getSupportFragmentManager(), mTitles, NO_OF_TABS);

        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        if (pager != null){
            pager.setAdapter(null);
            pager.setAdapter(mTabPageAdapter);
            pager.setCurrentItem(TAB_POSITION_1);
            PMGSlidingTabLayout tabs = (PMGSlidingTabLayout) findViewById(R.id.tabs);
            tabs.setViewPager(pager);
        }
    }

    private void setLocale(String lang)
    {
        Locale mLocale = new Locale(lang);
        Locale.setDefault(mLocale);
        Configuration config = new Configuration();
        config.locale = mLocale;

        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        getApplicationContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    public String getLanguagePreferences()
    {
        SharedPreferences settings =getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return settings.getString("selected_language","English");
    }

    public void setLanguagePreferences(String language)
    {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        // Writing data to SharedPreferences
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("selected_language", language);
        Log.d("Selected lang :", language);
        editor.commit();
    }
}

