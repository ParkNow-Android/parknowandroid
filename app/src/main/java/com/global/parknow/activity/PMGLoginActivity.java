package com.global.parknow.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.global.parknow.R;
import com.global.parknow.api.PMGLoginApiCalls;
import com.global.parknow.realm.PMGCreateAndUpdateUser;
import com.global.parknow.util.PMGCheckForProviders;
import com.global.parknow.util.PMGValidateEditText;

import io.fabric.sdk.android.Fabric;

public class PMGLoginActivity extends AppCompatActivity {

    private Switch switch_credentials;
    private EditText editText_Email,editText_Password;
    private Button button_Login;
    private String mUserId;
    private PMGLoginApiCalls mLoginApi;
    private PMGCheckForProviders mCheckProviders;
    private PMGValidateEditText mValidate;
    private PMGCreateAndUpdateUser mUserClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.ac_login);
        initiateScreenvalues();  // initiates screen views and values

        button_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonLoginClick();       // the functionality for  On click of login button
            }
        });
    }

    private void initiateScreenvalues(){
//        view declarations
        editText_Email = (EditText) findViewById(R.id.aclogin_editText_Email);
        editText_Password = (EditText) findViewById(R.id.aclogin_editText_Password);
        button_Login = (Button) findViewById(R.id.aclogin_button_Login);
        switch_credentials = (Switch) findViewById(R.id.aclogin_switch_credentials);
        Toolbar toolbar = (Toolbar) findViewById(R.id.aclogin_Toolbar);
        TextView textView_retrivePassword=(TextView)findViewById(R.id.aclogin_textView_retrievepassword);

//        class declarations
        mLoginApi = new PMGLoginApiCalls(getApplicationContext(),PMGLoginActivity.this);
        mCheckProviders = new PMGCheckForProviders(this);
        mValidate = new PMGValidateEditText(getApplicationContext());
        mUserClass = new PMGCreateAndUpdateUser(this);

//        other functions
        mUserId = mUserClass.CheckforUserId();
        if(mUserId.length() > 0) {
            editText_Email.setText(mUserId); // if userid exists set value to editText_email
            editText_Password.requestFocus();
        }

//        toolbar
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.mipmap.arrow_back_white_icon);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

//        Retrieve password functionality
        textView_retrivePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCheckProviders.checkForNetworkManager())
                    mLoginApi.showRetiervePasswordAlertDialog();
            }
        });
    }

    // called on click of button login
    private void buttonLoginClick() {
        boolean isEmailValid,isPasswordValid;
        String mEmailValue = editText_Email.getText().toString();
        String mPassword = editText_Password.getText().toString();

        isEmailValid = mValidate.Validate(editText_Email,mEmailValue,
                getResources().getString(R.string.userid_alert));
        isPasswordValid =  mValidate.Validate(editText_Password,mPassword,
                getResources().getString(R.string.password_alert));

        if(isEmailValid ) {
            if (isPasswordValid) {
                if (switch_credentials.isChecked() && !mUserId.equals(mEmailValue)) {
                    mUserClass.UpdateUserId();
                    mUserClass.createUserObject(mEmailValue);
                }
                if (mCheckProviders.checkForNetworkManager())
                    mLoginApi.getFirstSessionToken(mEmailValue, mPassword);
            }
        }

    }
}

