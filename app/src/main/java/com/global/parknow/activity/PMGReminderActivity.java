package com.global.parknow.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.global.parknow.R;

public class PMGReminderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        //To add toolbar to the screen
        setSupportActionBar(toolbar);
        //To add back button to Toolbar.
        toolbar.setNavigationIcon(R.mipmap.arrow_back_white_icon);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });


    }


}
