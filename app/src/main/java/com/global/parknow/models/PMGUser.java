package com.global.parknow.models;

import io.realm.RealmObject;

public class PMGUser extends RealmObject {
    private int     userId;
    private String  userName;
    private String  addressCity;
    private String  addressLine1;
    private String  addressLine2;
    private String  addressLine3;
    private String  addressLine4;
    private String  addressLine5;
    private String  addressZipcode;
    private String  supportedLanguages;
    private String  uiCulture;
    private int     clientId;
    private String  clientType;
    private String  countryCode;
    private String  email;
    private String  firstName;
    private String  lastName;
    private String  nickName;
    private boolean isMainUser;
    private boolean isSuspended;
    private String  localeIdentifier;
    private String  mobile;
    private int     supplierId;
    private String  suspendedCallToActionUrl;
    private String  suspendedMessage;
    private String  suspendedReason;


    public PMGUser()
    {
    }

    public String getSuspendedReason() {
        return suspendedReason;
    }

    public void setSuspendedReason(String mSuspendedReason) {
        this.suspendedReason = mSuspendedReason;
    }

    public String getSuspendedMessage() {
        return suspendedMessage;
    }

    public void setSuspendedMessage(String mSuspendedMessage) {
        this.suspendedMessage = mSuspendedMessage;
    }

    public String getSuspendedCallToActionUrl() {
        return suspendedCallToActionUrl;
    }

    public void setSuspendedCallToActionUrl(String mSuspendedCallToActionUrl) {
        this.suspendedCallToActionUrl = mSuspendedCallToActionUrl;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int mSupplierId) {
        this.supplierId = mSupplierId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String mNickName) {
        this.nickName = mNickName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mMobile) {
        this.mobile = mMobile;
    }

    public String getLocaleIdentifier() {
        return localeIdentifier;
    }

    public void setLocaleIdentifier(String mLocaleIdentifier) {
        this.localeIdentifier = mLocaleIdentifier;
    }

    public boolean isIsSuspended() {
        return isSuspended;
    }

    public void setIsSuspended(boolean mIsSuspended) {
        this.isSuspended = mIsSuspended;
    }

    public boolean isIsMainUser() {
        return isMainUser;
    }

    public void setIsMainUser(boolean mIsMainUser) {
        this.isMainUser = mIsMainUser;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String mLastName) {
        this.lastName = mLastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String mFirstName) {
        this.firstName = mFirstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String mEmail) {
        this.email = mEmail;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String mCountryCode) {
        this.countryCode = mCountryCode;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String mClientType) {
        this.clientType = mClientType;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int mClientId) {
        this.clientId = mClientId;
    }

    public String getUiCulture() {
        return uiCulture;
    }

    public void setUiCulture(String mUiCulture) {
        this.uiCulture = mUiCulture;
    }

    public String getSupportedLanguages() {
        return supportedLanguages;
    }

    public void setSupportedLanguages(String mSupportedLanguages) {
        this.supportedLanguages = mSupportedLanguages;
    }

    public String getAddressZipcode() {
        return addressZipcode;
    }

    public void setAddressZipcode(String mAddressZipcode) {
        this.addressZipcode = mAddressZipcode;
    }

    public String getAddressLine5() {
        return addressLine5;
    }

    public void setAddressLine5(String mAddressLine5) {
        this.addressLine5 = mAddressLine5;
    }

    public String getAddressLine4() {
        return addressLine4;
    }

    public void setAddressLine4(String mAddressLine4) {
        this.addressLine4 = mAddressLine4;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String mAddressLine3) {
        this.addressLine3 = mAddressLine3;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String mAddressLine2) {
        this.addressLine2 = mAddressLine2;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String mAddressLine1) {
        this.addressLine1 = mAddressLine1;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String mAddressCity) {
        this.addressCity = mAddressCity;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String mUserName) {
        this.userName = mUserName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int mUserId) {
        this.userId = mUserId;
    }


}
