package com.global.parknow.models;

/**
 * Created by shwetha.v on 6/16/2015.
 */
public class PMGAuthenticationLogin {
    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTokenExpirationUtc() {
        return tokenExpirationUtc;
    }

    public void setTokenExpirationUtc(String tokenExpirationUtc) {
        this.tokenExpirationUtc = tokenExpirationUtc;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    private String refreshToken,token,tokenExpirationUtc,clientId,userId,supplierId,email,mobileNumber;
}
