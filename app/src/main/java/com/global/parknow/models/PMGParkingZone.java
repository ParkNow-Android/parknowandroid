package com.global.parknow.models;

import java.util.Date;

import io.realm.RealmObject;

public class PMGParkingZone extends RealmObject{

    private int     parkingActionId;
    private String  internalZoneCode;
    private int     locationZoneCode;
    private String  locationName;
    private int     locationCode;
    private boolean isParkingAllowed;
    private boolean isPayBySpace;
    private boolean isRequiredSpaceValidation;
    private boolean isStartDuration;
    private Date    maxStopTimeLocal;
    private Date    startTimeLocal;
    private Date    maxParkingTime;
    private Date    maxStopTimeUtc;
    private float   latitude;
    private float   longitude;
    private float   radius;
    private String  signageCode;
    private String  type;
    private int     zoneId;
    private int     typeId;

    public PMGParkingZone()
    {
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int mTypeId) {
        this.typeId = mTypeId;
    }

    public int getZoneId() {
        return zoneId;
    }

    public void setZoneId(int mZoneId) {
        this.zoneId = mZoneId;
    }

    public String getType() {
        return type;
    }

    public void setType(String mType) {
        this.type = mType;
    }

    public String getSignageCode() {
        return signageCode;
    }

    public void setSignageCode(String mSignageCode) {
        this.signageCode = mSignageCode;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float mRadius) {
        this.radius = mRadius;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float mLongitude) {
        this.longitude = mLongitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float mLatitude) {
        this.latitude = mLatitude;
    }

    public Date getStartTimeLocal() {
        return startTimeLocal;
    }

    public void setStartTimeLocal(Date mStartTimeLocal) {
        this.startTimeLocal = mStartTimeLocal;
    }

    public Date getMaxStopTimeUtc() {
        return maxStopTimeUtc;
    }

    public void setMaxStopTimeUtc(Date mMaxStopTimeUtc) {
        this.maxStopTimeUtc = mMaxStopTimeUtc;
    }

    public Date getMaxParkingTime() {
        return maxParkingTime;
    }

    public void setMaxParkingTime(Date mMaxParkingTime) {
        this.maxParkingTime = mMaxParkingTime;
    }

    public Date getMaxStopTimeLocal() {
        return maxStopTimeLocal;
    }

    public void setMaxStopTimeLocal(Date mMaxStopTimeLocal) {
        this.maxStopTimeLocal = mMaxStopTimeLocal;
    }

    public boolean isIsStartDuration() {
        return isStartDuration;
    }

    public void setIsStartDuration(boolean mIsStartDuration) {
        this.isStartDuration = mIsStartDuration;
    }

    public boolean isIsRequiredSpaceValidation() {
        return isRequiredSpaceValidation;
    }

    public void setIsRequiredSpaceValidation(boolean mIsRequiredSpaceValidation) {
        this.isRequiredSpaceValidation = mIsRequiredSpaceValidation;
    }

    public boolean isIsPayBySpace() {
        return isPayBySpace;
    }

    public void setIsPayBySpace(boolean mIsPayBySpace) {
        this.isPayBySpace = mIsPayBySpace;
    }

    public boolean isIsParkingAllowed() {
        return isParkingAllowed;
    }

    public void setIsParkingAllowed(boolean mIsParkingAllowed) {
        this.isParkingAllowed = mIsParkingAllowed;
    }

    public int getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(int mLocationCode) {
        this.locationCode = mLocationCode;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String mLocationName) {
        this.locationName = mLocationName;
    }

    public int getLocationZoneCode() {
        return locationZoneCode;
    }

    public void setLocationZoneCode(int mLocationZoneCode) {
        this.locationZoneCode = mLocationZoneCode;
    }

    public String getInternalZoneCode() {
        return internalZoneCode;
    }

    public void setInternalZoneCode(String mInternalZoneCode) {
        this.internalZoneCode = mInternalZoneCode;
    }

    public int getParkingActionId() {
        return parkingActionId;
    }

    public void setParkingActionId(int mParkingActionId) {
        this.parkingActionId = mParkingActionId;
    }
}
