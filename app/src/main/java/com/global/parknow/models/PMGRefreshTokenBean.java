package com.global.parknow.models;

/**
 * Created by shwetha.v on 6/18/2015.
 */
public class PMGRefreshTokenBean {
    private String refreshtoken;

    public PMGRefreshTokenBean(String refreshtoken) {
        this.refreshtoken = refreshtoken;
    }

}
