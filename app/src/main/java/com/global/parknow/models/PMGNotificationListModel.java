package com.global.parknow.models;


public class PMGNotificationListModel {

    private String notification_active_list_content;
    private String notification_active_list_image;
    private String notification_inactive_list_content;
    private String notification_inactive_list_image;

    public String getNotification_active_list_content() {
        return notification_active_list_content;
    }

    public void setNotification_active_list_content(String notification_active_list_content) {
        this.notification_active_list_content = notification_active_list_content;
    }

    public String getNotification_active_list_image() {
        return notification_active_list_image;
    }

    public void setNotification_active_list_image(String notification_active_list_image) {
        this.notification_active_list_image = notification_active_list_image;
    }

    public String getNotification_inactive_list_content() {
        return notification_inactive_list_content;
    }

    public void setNotification_inactive_list_content(String notification_inactive_list_content) {
        this.notification_inactive_list_content = notification_inactive_list_content;
    }

    public String getNotification_inactive_list_image() {
        return notification_inactive_list_image;
    }

    public void setNotification_inactive_list_image(String notification_inactive_list_image) {
        this.notification_inactive_list_image = notification_inactive_list_image;
    }
}
