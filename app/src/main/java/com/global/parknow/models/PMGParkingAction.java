package com.global.parknow.models;

import java.util.Date;

import io.realm.RealmObject;

public class PMGParkingAction extends RealmObject {



    private boolean canExtend;
    private boolean canStop;
    private int     countType;
    private String  customerNote;
    private int     parkingZoneId;
    private int     masterId;
    private Date    nowLocal;
    private Date    nowUtc;
    private String  spaceNumber;
    private String  timeZoneStandardName;
    private Date    startLocal;
    private Date    stopLocal;
    private Date    startUtc;
    private Date    stopUtc;
    private int     cvcCode;
    private boolean isActive;
    private boolean isPending;
    private double  latitude;
    private double  longitude;
    private Date    nowTimeUtc;

    public PMGParkingAction()
    {
    }

    public boolean isCanExtend() {
        return canExtend;
    }

    public void setCanExtend(boolean mCanExtend) {
        this.canExtend = mCanExtend;
    }

    public boolean isCanStop() {
        return canStop;
    }

    public void setCanStop(boolean mCanStop) {
        this.canStop = mCanStop;
    }

    public int getCountType() {
        return countType;
    }

    public void setCountType(int mCountType) {
        this.countType = mCountType;
    }

    public String getCustomerNote() {
        return customerNote;
    }

    public void setCustomerNote(String mCustomerNote) {
        this.customerNote = mCustomerNote;
    }

    public int getParkingZoneId() {
        return parkingZoneId;
    }

    public void setParkingZoneId(int mParkingZoneId) {
        this.parkingZoneId = mParkingZoneId;
    }

    public int getMasterId() {
        return masterId;
    }

    public void setMasterId(int mMasterId) {
        this.masterId = mMasterId;
    }

    public Date getNowLocal() {
        return nowLocal;
    }

    public void setNowLocal(Date mNowLocal) {
        this.nowLocal = mNowLocal;
    }

    public Date getNowUtc() {
        return nowUtc;
    }

    public void setNowUtc(Date mNowUtc) {
        this.nowUtc = mNowUtc;
    }

    public String getSpaceNumber() {
        return spaceNumber;
    }

    public void setSpaceNumber(String mSpaceNumber) {
        this.spaceNumber = mSpaceNumber;
    }

    public String getTimeZoneStandardName() {
        return timeZoneStandardName;
    }

    public void setTimeZoneStandardName(String mTimeZoneStandardName) {
        this.timeZoneStandardName = mTimeZoneStandardName;
    }

    public Date getStopLocal() {
        return stopLocal;
    }

    public void setStopLocal(Date mStopLocal) {
        this.stopLocal = mStopLocal;
    }

    public Date getStartUtc() {
        return startUtc;
    }

    public void setStartUtc(Date mStartUtc) {
        this.startUtc = mStartUtc;
    }

    public Date getStopUtc() {
        return stopUtc;
    }

    public void setStopUtc(Date mStopUtc) {
        this.stopUtc = mStopUtc;
    }


    public void setStartLocal(Date mStartLocal) {
        this.startLocal = mStartLocal;
    }

    public Date getStartLocal(){
        return startLocal;
    }

    public int getCvcCode() {
        return cvcCode;
    }

    public void setCvcCode(int mCvcCode) {
        this.cvcCode = mCvcCode;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean mIsActive) {
        this.isActive = mIsActive;
    }

    public boolean isIsPending() {
        return isPending;
    }

    public void setIsPending(boolean mIsPending) {
        this.isPending = mIsPending;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double mLatitude) {
        this.latitude = mLatitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double mLongitude) {
        this.longitude = mLongitude;
    }

    public Date getNowTimeUtc() {
        return nowTimeUtc;
    }

    public void setNowTimeUtc(Date mNowTimeUtc) {
        this.nowTimeUtc = mNowTimeUtc;
    }
}
