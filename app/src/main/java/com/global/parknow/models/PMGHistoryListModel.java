package com.global.parknow.models;

public class PMGHistoryListModel {


    private String history_Parking_Zone;
    private String history_Parking_Place;
    private String history_Parking_day;
    private String history_Parking_Start_Hr;
    private String history_Parking_End_Hr;
    private String history_Parking_Vehicle_No;
    private String history_Parking_Price;

    public String getHistory_Parking_Zone() {
        return history_Parking_Zone;
    }

    public void setHistory_Parking_Zone(String history_Parking_Zone) {
        this.history_Parking_Zone = history_Parking_Zone;
    }

    public String getHistory_Parking_Place() {
        return history_Parking_Place;
    }

    public void setHistory_Parking_Place(String history_Parking_Place) {
        this.history_Parking_Place = history_Parking_Place;
    }

    public String getHistory_Parking_day() {
        return history_Parking_day;
    }

    public void setHistory_Parking_day(String history_Parking_day) {
        this.history_Parking_day = history_Parking_day;
    }

    public String getHistory_Parking_Start_Hr() {
        return history_Parking_Start_Hr;
    }

    public void setHistory_Parking_Start_Hr(String history_Parking_Start_Hr) {
        this.history_Parking_Start_Hr = history_Parking_Start_Hr;
    }

    public String getHistory_Parking_End_Hr() {
        return history_Parking_End_Hr;
    }

    public void setHistory_Parking_End_Hr(String history_Parking_End_Hr) {
        this.history_Parking_End_Hr = history_Parking_End_Hr;
    }

    public String getHistory_Parking_Vehicle_No() {
        return history_Parking_Vehicle_No;
    }

    public void setHistory_Parking_Vehicle_No(String history_Parking_Vehicle_No) {
        this.history_Parking_Vehicle_No = history_Parking_Vehicle_No;
    }

    public String getHistory_Parking_Price() {
        return history_Parking_Price;
    }

    public void setHistory_Parking_Price(String history_Parking_Price) {
        this.history_Parking_Price = history_Parking_Price;
    }
}
