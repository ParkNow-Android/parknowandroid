package com.global.parknow.models;

import io.realm.RealmObject;

public class PMGPriceDetail extends RealmObject {
    private String  billingType;
    private double  parkingPrice;
    private double  parkingPriceExVatAmount;
    private double  parkingVat;
    private float   parkingVatPercentage;
    private double  serviceFee;
    private double  serviceFeeVat;
    private float   serviceFeeVatPercentage;
    private boolean showVat;
    private double  totalPrice;

    public PMGPriceDetail()
    {
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double mTotalPrice) {
        this.totalPrice = mTotalPrice;
    }

    public boolean isShowVat() {
        return showVat;
    }

    public void setShowVat(boolean mShowVat) {
        this.showVat = mShowVat;
    }

    public float getServiceFeeVatPercentage() {
        return serviceFeeVatPercentage;
    }

    public void setServiceFeeVatPercentage(float mServiceFeeVatPercentage) {
        this.serviceFeeVatPercentage = mServiceFeeVatPercentage;
    }

    public double getServiceFeeVat() {
        return serviceFeeVat;
    }

    public void setServiceFeeVat(double mServiceFeeVat) {
        this.serviceFeeVat = mServiceFeeVat;
    }

    public double getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(double mServiceFee) {
        this.serviceFee = mServiceFee;
    }

    public float getParkingVatPercentage() {
        return parkingVatPercentage;
    }

    public void setParkingVatPercentage(float mParkingVatPercentage) {
        this.parkingVatPercentage = mParkingVatPercentage;
    }

    public double getParkingVat() {
        return parkingVat;
    }

    public void setParkingVat(double mParkingVat) {
        this.parkingVat = mParkingVat;
    }

    public double getParkingPriceExVatAmount() {
        return parkingPriceExVatAmount;
    }

    public void setParkingPriceExVatAmount(double mParkingPriceExVatAmount) {
        this.parkingPriceExVatAmount = mParkingPriceExVatAmount;
    }

    public double getParkingPrice() {
        return parkingPrice;
    }

    public void setParkingPrice(double mParkingPrice) {
        this.parkingPrice = mParkingPrice;
    }

    public String getBillingType() {
        return billingType;
    }

    public void setBillingType(String mBillingType) {
        this.billingType = mBillingType;
    }
}
