package com.global.parknow.models;

import java.util.Date;

import io.realm.RealmObject;

public class PMGVehicle extends RealmObject {

    private int     vehicleId;
    private String  cardNumber;
    private int     cardStatus;
    private String  countyCode;
    private int     identifier;
    private boolean isDefault;
    private Date    lastUsedInActive;
    private String  state;
    private String  userDescription;
    private String  vrn;

    public PMGVehicle()
    {
    }

    public String getVrn() {
        return vrn;
    }

    public void setVrn(String mVrn) {
        this.vrn = mVrn;
    }

    public String getUserDescription() {
        return userDescription;
    }

    public void setUserDescription(String mUserDescription) {
        this.userDescription = mUserDescription;
    }

    public String getState() {
        return state;
    }

    public void setState(String mState) {
        this.state = mState;
    }

    public Date getLastUsedInActive() {
        return lastUsedInActive;
    }

    public void setLastUsedInActive(Date mLastUsedInActive) {
        this.lastUsedInActive = mLastUsedInActive;
    }

    public boolean isIsDefault() {
        return isDefault;
    }

    public void setIsDefault(boolean mIsDefault) {
        this.isDefault = mIsDefault;
    }

    public int getIdentifier() {
        return identifier;
    }

    public void setIdentifier(int mIdentifier) {
        this.identifier = mIdentifier;
    }

    public String getCountyCode() {
        return countyCode;
    }

    public void setCountyCode(String mCountyCode) {
        this.countyCode = mCountyCode;
    }

    public int getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(int mCardStatus) {
        this.cardStatus = mCardStatus;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String mCardNumber) {
        this.cardNumber = mCardNumber;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int mVehicleId) {
        this.vehicleId = mVehicleId;
    }
}
