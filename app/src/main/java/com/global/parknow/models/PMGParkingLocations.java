package com.global.parknow.models;

import java.util.Date;
import io.realm.RealmObject;

public class PMGParkingLocations extends RealmObject {

    //private int supplierId;
    private String  name;
    private boolean isCardEnforced;
    private boolean isStickerEnforcerd;
    private String  kmlUrlString;
    private String  prefix;
    private int     numberOfSpaces;
    private String  country;
    private boolean hasGarages;
    private String  infoName;
    private String  infoUrl;
    private Date    infoLastModifiedUtc;
    private String  infoZoneCodePrefix;

    public PMGParkingLocations()
    {
    }

    public String getInfoZoneCodePrefix() {
        return infoZoneCodePrefix;
    }

    public void setInfoZoneCodePrefix(String mInfoZoneCodePrefix) {
        this.infoZoneCodePrefix = mInfoZoneCodePrefix;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String mCountry) {
        this.country = mCountry;
    }

    public int getNumberOfSpaces() {
        return numberOfSpaces;
    }

    public void setNumberOfSpaces(int mNumberOfSpaces) {
        this.numberOfSpaces = mNumberOfSpaces;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String mPrefix) {
        this.prefix = mPrefix;
    }

    public String getKmlUrlString() {
        return kmlUrlString;
    }

    public void setKmlUrlString(String mKmlUrlString) {
        this.kmlUrlString = mKmlUrlString;
    }

    //    public int getSupplierId() {
//        return supplierId;
//    }
//
//    public void setSupplierId(int supplierId) {
//        this.supplierId = supplierId;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCardEnforced() {
        return isCardEnforced;
    }

    public void setCardEnforced(boolean isCardEnforced) {
        this.isCardEnforced = isCardEnforced;
    }

    public boolean isHasGarages() {
        return hasGarages;
    }

    public void setHasGarages(boolean hasGarages) {
        this.hasGarages = hasGarages;
    }

    public boolean isStickerEnforcerd() {
        return isStickerEnforcerd;
    }

    public void setStickerEnforcerd(boolean isStickerEnforcerd) {
        this.isStickerEnforcerd = isStickerEnforcerd;
    }


//    Information field has to be added here after the confirmation of datatype

    public String getInfoName() {
        return infoName;
    }

    public void setInfoName(String infoName) {
        this.infoName = infoName;
    }

    public Date getInfoLastModifiedUtc() {
        return infoLastModifiedUtc;
    }

    public void setInfoLastModifiedUtc(Date infoLastModifiedUtc) {
        this.infoLastModifiedUtc = infoLastModifiedUtc;
    }

    public String getInfoUrl() {
        return infoUrl;
    }

    public void setInfoUrl(String infoUrl) {
        this.infoUrl = infoUrl;
    }

}
