package com.global.parknow.models;

import java.util.Date;

import io.realm.RealmObject;

public class PMGParkingLocation extends RealmObject {

    private boolean isCardEnforced;
    private boolean isStickerEnforced;
    private String  kmlUrlString;
    private String  name;
    private String  prefix;
    private int     numberOfSpace;
    private String  country;
    private boolean hasGarages;
    private String  infoName;
    private String  infoUrl;
    private Date    infoLastModifiedUtc;
    private String  infoZoneCodePrefix;

    public PMGParkingLocation()
    {
    }

    public String getInfoZoneCodePrefix() {
        return infoZoneCodePrefix;
    }

    public void setInfoZoneCodePrefix(String mInfoZoneCodePrefix) {
        this.infoZoneCodePrefix = mInfoZoneCodePrefix;
    }

    public Date getInfoLastModifiedUtc() {
        return infoLastModifiedUtc;
    }

    public void setInfoLastModifiedUtc(Date mInfoLastModifiedUtc) {
        this.infoLastModifiedUtc = mInfoLastModifiedUtc;
    }

    public String getInfoUrl() {
        return infoUrl;
    }

    public void setInfoUrl(String mInfoUrl) {
        this.infoUrl = mInfoUrl;
    }

    public String getInfoName() {
        return infoName;
    }

    public void setInfoName(String mInfoName) {
        this.infoName = mInfoName;
    }

    public boolean isHasGarages() {
        return hasGarages;
    }

    public void setHasGarages(boolean mHasGarages) {
        this.hasGarages = mHasGarages;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String mCountry) {
        this.country = mCountry;
    }

    public int getNumberOfSpace() {
        return numberOfSpace;
    }

    public void setNumberOfSpace(int mNumberOfSpace) {
        this.numberOfSpace = mNumberOfSpace;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String mPrefix) {
        this.prefix = mPrefix;
    }

    public String getName() {
        return name;
    }

    public void setName(String mName) {
        this.name = mName;
    }

    public String getKmlUrlString() {
        return kmlUrlString;
    }

    public void setKmlUrlString(String mKmlUrlString) {
        this.kmlUrlString = mKmlUrlString;
    }

    public boolean isIsStickerEnforced() {
        return isStickerEnforced;
    }

    public void setIsStickerEnforced(boolean mIsStickerEnforced) {
        this.isStickerEnforced = mIsStickerEnforced;
    }







    public boolean isIsCardEnforced() {
        return isCardEnforced;
    }

    public void setIsCardEnforced(boolean mIsCardEnforced) {
        this.isCardEnforced = mIsCardEnforced;
    }



}
