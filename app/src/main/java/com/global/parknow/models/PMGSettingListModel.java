package com.global.parknow.models;

public class PMGSettingListModel {

    private String setting_list_content;
    private String setting_list_image;
    private String setting_lang_list_content;
    private String setting_lang_list_image;



    public String getSetting_list_content() {
        return setting_list_content;
    }

    public void setSetting_list_content(String setting_list_content) {
        this.setting_list_content = setting_list_content;
    }

    public String getSetting_list_image() {
        return setting_list_image;
    }

    public void setSetting_list_image(String setting_list_image) {
        this.setting_list_image = setting_list_image;
    }

    public String getSetting_lang_list_content() {
        return setting_lang_list_content;
    }

    public void setSetting_lang_list_content(String setting_lang_list_content) {
        this.setting_lang_list_content = setting_lang_list_content;
    }

    public String getSetting_lang_list_image() {
        return setting_lang_list_image;
    }

    public void setSetting_lang_list_image(String setting_lang_list_image) {
        this.setting_lang_list_image = setting_lang_list_image;
    }
}
