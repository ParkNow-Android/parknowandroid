package com.global.parknow.models;

import io.realm.RealmObject;

public class PMGInvoice extends RealmObject{

    private int    count;
    private String title;
    private int    currentPage;
    private int    totalPage;
    private int    nextPage;
    private float  amount;
    private String currencySymbol;
    private String documentURL;
    private String statusDescription;
    private String invoiceStatus;

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String mInvoiceStatus) {
        this.invoiceStatus = mInvoiceStatus;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String mStatusDescription) {
        this.statusDescription = mStatusDescription;
    }

    public String getDocumentURL() {
        return documentURL;
    }

    public void setDocumentURL(String mDocumentURL) {
        this.documentURL = mDocumentURL;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String mCurrencySymbol) {
        this.currencySymbol = mCurrencySymbol;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float mAmount) {
        this.amount = mAmount;
    }

    public int getNextPage() {
        return nextPage;
    }

    public void setNextPage(int mNextPage) {
        this.nextPage = mNextPage;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int mTotalPage) {
        this.totalPage = mTotalPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int mCurrentPage) {
        this.currentPage = mCurrentPage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String mTitle) {
        this.title = mTitle;
    }



    public int getCount() {
        return count;
    }

    public void setCount(int mCount) {
        this.count = mCount;
    }

}
