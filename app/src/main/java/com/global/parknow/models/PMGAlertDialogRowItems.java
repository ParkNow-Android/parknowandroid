package com.global.parknow.models;


public class PMGAlertDialogRowItems {

    private String content;
    private int imageId;


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
